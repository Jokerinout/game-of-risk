package fr.alma.risk;

import fr.alma.risk.entity.Link;
import fr.alma.risk.entity.Territoire;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertTrue;

public class LinkTest {
    private Link link;
    private Territoire deb;
    private Territoire fin;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        link = Mockito.mock(Link.class);
    }

    @Test
    public void testGetIdLien() {
        Mockito.when(link.getIdLien()).thenReturn(1, 30);

        assertTrue(link.getIdLien() == 1);
        assertTrue(link.getIdLien() == 30);
    }

    @Test
    public void testGetTerritoire_deb(){
        Mockito.when(link.getTerritoire_deb()).thenReturn(deb);

        assertTrue(link.getTerritoire_deb() == deb);


    }

    @Test
    public void testGetTerritoire_fin() {
        Mockito.when(link.getTerritoire_deb()).thenReturn(fin);

        assertTrue(link.getTerritoire_deb() == fin);

    }

}