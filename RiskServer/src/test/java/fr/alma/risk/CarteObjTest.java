package fr.alma.risk;

import fr.alma.risk.entity.CarteObj;
import fr.alma.risk.repository.CarteObjRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.SpringApplication;

import static org.junit.Assert.assertTrue;




public class CarteObjTest {
    private CarteObj carteObj;
    private CarteObjRepository carteObjRepository;

    @Before
    public void setUp() {
       /* String[] args = new String[];
        SpringApplication.run(AccessingDataMysqlApplication.class, args);
*/
        MockitoAnnotations.initMocks(this);
        carteObj = Mockito.mock(CarteObj.class);
        carteObjRepository = Mockito.mock(CarteObjRepository.class);
       // Mockito.when(carteObjRepository.findAll()).thenReturn();

    }

    @Test
    public void testGetIdCartObj(){


        Mockito.when(carteObj.getIdCarteObj()).thenReturn(1);

        //Mockito.when(carteObjRepository.findAll()).thenReturn()
        assertTrue(carteObj.getIdCarteObj() == 1);
    }

    @Test
    public void testGetDescription(){

        Mockito.when(carteObj.getDescription()).thenReturn("Vous devez conquerir 24 territoires au choix");

        assertTrue(carteObj.getDescription() == "Vous devez conquerir 24 territoires au choix");
    }

    @Test
    public void testGetContinent1(){

        Mockito.when(carteObj.getContinent1()).thenReturn("4", null);

        assertTrue(carteObj.getContinent1() == "4");
        assertTrue(carteObj.getContinent1() == null);

    }

    @Test
    public void testGetContinent2() {
        Mockito.when(carteObj.getContinent2()).thenReturn("5",null);

        assertTrue(carteObj.getContinent2() == "5");
        assertTrue(carteObj.getContinent2() == null);
    }

    @Test
    public void testGetCouleur() {
        Mockito.when(carteObj.getCouleur()).thenReturn("rouge", "jaune");

        assertTrue(carteObj.getCouleur() == "rouge");
        assertTrue(carteObj.getCouleur() == "jaune");
    }

    @Test
    public void testGetNbTerri() {
        Mockito.when(carteObj.getNbTerri()).thenReturn("3", "25");

        assertTrue(carteObj.getNbTerri() == "3");
        assertTrue(carteObj.getNbTerri() == "25");
    }

    @Test
    public void testGetCond() {
        Mockito.when(carteObj.getCond()).thenReturn("pays min 2 armee", null, "+1 continent");

        assertTrue(carteObj.getCond() == "pays min 2 armee");
        assertTrue(carteObj.getCond() == null);
        assertTrue(carteObj.getCond() == "+1 continent");
    }

    @Test
    public void testGetType(){
        Mockito.when(carteObj.getType()).thenReturn("conquerir", "detruire");

        assertTrue(carteObj.getType() == "conquerir");
        assertTrue(carteObj.getType() == "detruire");
    }



}
