package fr.alma.risk;

import fr.alma.risk.entity.*;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


/*essaie de faire un mock du repository */
public class UserTest {
    private User user;
    private User userMock;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        userMock = Mockito.mock(User.class);
        user = new User("blue", "bastien");

    }

    @Test
    public void testCreateUser(){

    }

    @Test
    public void testGetColor() {

        assertTrue(user.getColor() == "blue");
    }

    @Test
    public void testGetSessionId(){

        assertTrue(user.getSessionId() == "bastien");
    }

    @Test
    public void testGetGame() {
        List<User> players = new ArrayList<>();
        players.add(new User("blue", "bastien"));
        List<CarteTerr> terr = new ArrayList<>();
        List<CarteObj> obj = new ArrayList<>();
        Game g = new Game(players, obj, terr);

        Mockito.when(userMock.getGame()).thenReturn(g);
        assertTrue(userMock.getGame() == g);
    }

    @Test
    public void testSetGame() {
        List<User> players = new ArrayList<>();
        players.add(new User("blue", "bastien"));
        List<CarteTerr> terr = new ArrayList<>();
        List<CarteObj> obj = new ArrayList<>();
        Game g = new Game(players, obj, terr);

        user.setGame(g);

        assertTrue(user.getGame() == g);
    }


    @Test
    public void testGetTabCont() {
        //faire un cas de test avec un list de tabcont et non tab vide
        assertEquals(user.getTabCont() ,new ArrayList<>());
    }

    @Test
    public void testGetTabTerri() {
        //faire un cas de test avec un list de tabcont et non tab vide
        assertEquals(user.getTabTerri() ,new ArrayList<>());
    }

    @Test
    public void testAddContinent() {
        Continent continent;

        continent = new Continent();
        user.addCont(continent);

        assertTrue(user.getTabCont().contains(continent));
    }

    @Test
    public void testRemoveContinent() {
        Continent continent1;
        Continent continent2;

        continent1 = new Continent();
        continent2 = new Continent();

        user.addCont(continent1);
        user.addCont(continent2);

        user.removeContByName(continent1.getName());
        //on verif que continent2 est encre present et que continent2 ne l'est plus
        assertTrue(user.getTabCont().contains(continent2));
        assertFalse(user.getTabCont().contains(continent1));

    }

    @Test
    public void testContainsCont(){
        Mockito.when(userMock.containsCont(5)).thenReturn(true);

        assertTrue(userMock.containsCont(5));
        assertFalse(user.containsCont(5));
    }

    @Test
    public void testFindTerriByName() {
        Territoire terri;
        terri = new Territoire();

        Mockito.when(userMock.findTerrByName("Congo")).thenReturn(terri);
        assertEquals(userMock.findTerrByName("Congo"), terri);

        assertEquals(user.findTerrByName("Congo"), null);
    }

    @Test
    public void testFindTerriById(){
        Territoire terri;
        terri = new Territoire();

        Mockito.when(userMock.findTerr(5)).thenReturn(terri);
        assertEquals(userMock.findTerr(5), terri);

        assertEquals(user.findTerr(5), null);
    }

    @Test
    public void testContainsTerritoireByName(){
        Territoire terri;
        terri = new Territoire();

        Mockito.when(userMock.containsTerr("Chine")).thenReturn(true);
        assertTrue(userMock.containsTerr("Chine"));
        assertFalse(user.containsTerr("Chine"));
    }

    @Test
    public void testContainsTerritoireById(){
        Territoire terri;
        terri = new Territoire();

        Mockito.when(userMock.containsTerrById(3)).thenReturn(true);
        assertTrue(userMock.containsTerrById(3));
        assertFalse(user.containsTerrById(3));
    }

    @Test
    public void testRemoveTerrritoireWithParameterIsTerritoire() {
        Territoire t1;
        Territoire t2;
        t1 = new Territoire();
        t2 = new Territoire();

        user.addTerr(t1);
        user.addTerr(t2);

        user.removeTerr(t1);
        assertTrue(user.getTabTerri().contains(t2));
        assertFalse(user.getTabTerri().contains(t1));
    }

    @Test
    public void testRemoveTerritoireById() {
        Territoire terri;
        terri = new Territoire();

        Mockito.when(userMock.removeTerrByTerrId(5)).thenReturn(terri);
        assertEquals(userMock.removeTerrByTerrId(5), terri);
        assertEquals(user.removeTerrByTerrId(5), null);
    }

    @Test
    public void testGetMyCarteObj() {
        CarteObj carteObj;
        carteObj = new CarteObj();

        Mockito.when(userMock.getMyCardObj()).thenReturn(carteObj);
        assertEquals(userMock.getMyCardObj(),carteObj);
    }

    @Test
    public void testSetMyCarteObj(){
        CarteObj carteObj;
        carteObj = new CarteObj();

        assertEquals(user.getMyCardObj(), null);

        user.setCardObj(carteObj);
        assertEquals(user.getMyCardObj(), carteObj);
    }

    @Test
    public void testGetMyCardsTerritoire(){
        assertEquals(user.getMyCardsTerr(), new ArrayList<>());

        //essayer de fare un cas avec un vrai list
    }

    @Test
    public void testAddCardTerritoite(){
        CarteTerr carteTerr;
        carteTerr = new CarteTerr();

        user.addCardTerr(carteTerr);
        assertTrue(user.getMyCardsTerr().contains(carteTerr));

    }

    @Test
    public void testSearcCardTerritoireById(){
        Mockito.when(userMock.searchCardTerr(9)).thenReturn(true);
        assertTrue(userMock.searchCardTerr(9));
        assertFalse(user.searchCardTerr(9));
    }

    @Test
    public void testRemoveCardTerritoire() {
        CarteTerr carteTerr;
        carteTerr = new CarteTerr();

        // arme a mettre en coherence avec ce que l'on doit mettre en parametre
        Mockito.when(userMock.removeCardTerr("arme")).thenReturn(carteTerr) ;
        assertEquals(userMock.removeCardTerr("arme"), carteTerr);
        assertEquals(user.removeCardTerr("arme"), null);
    }

    @Test
    public void testOccurencyCard() {
        //simulation du nmbre d'occurence à l'aide d'un mock sinon la valeur de retour est 0 sans
        assertEquals(user.occurencyCard("arme"), 0);
        Mockito.when(userMock.occurencyCard("arme")).thenReturn(5);
        assertEquals(userMock.occurencyCard("arme"), 5);
    }

    @Test
    public void testGetNbArmees() {
        //simulation avec le mock pour voir grandire le nb d'armees sinon 0
        assertEquals(user.getNbArmeeTotal(), 0);
        Mockito.when(userMock.getNbArmeeTotal()).thenReturn(50);
        assertEquals(userMock.getNbArmeeTotal(), 50);
    }

    @Test
    public void testChargeArmyReturnZero() {
        assertEquals(user.changeArmy(0),0);
    }

    @Test
    public void testChargeAmyReturnSomethingElseThanZero(){
        assertEquals(user.changeArmy(50), 50);
    }

    @Test
    public void testGetArmeeAPlacer(){
        assertEquals(user.getArmeeAplacer(), 0);
    }

    @Test
    public void testSetArmeeAPlacer() {
        user.setArmeeAplacer(30);
        assertEquals(user.getArmeeAplacer(), 30);
    }

    @Test
    public void testIncrementArmeeAPlacer() {
        user.setArmeeAplacer(30);
        user.incrementArmeeAplacer(5);
        assertEquals(user.getArmeeAplacer(), 35);
    }

    @Test
    public void testPlayerIsAlive(){
        assertTrue(user.getIsAlive());

    }

    @Test
    public void testEliminatePlayer(){
        user.elimine();
        assertFalse(user.getIsAlive());
    }

    @Test
    public void testGetPassTurn() {
        assertFalse(user.getPassTurn());
    }

    @Test
    public void testChangePassTurn(){
        user.changePassTurn();
        assertTrue(user.getPassTurn());

        user.changePassTurn();
        assertFalse(user.getPassTurn());
    }

}