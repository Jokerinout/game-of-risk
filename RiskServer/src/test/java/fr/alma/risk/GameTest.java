package fr.alma.risk;

import fr.alma.risk.entity.CarteObj;
import fr.alma.risk.entity.CarteTerr;
import fr.alma.risk.entity.Game;
import fr.alma.risk.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class GameTest {
    private Game game;
    private Game gameMock;
    private User user;
    private List<User> players;
    private List<CarteTerr> terr;
    private List<CarteObj> obj;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        gameMock = Mockito.mock(Game.class);

        players = new ArrayList<>();
        user = new User("blue", "bastien");
        players.add(user);
        terr = new ArrayList<>();
        obj = new ArrayList<>();
        game = new Game(players, obj, terr);
    }

    @Test
    public void testGetWinner(){
        assertEquals(game.getWinner(), "");
    }

    @Test
    public  void testSetWinner(){
        game.setWinner("Bastien");
        assertEquals(game.getWinner(), "Bastien");
    }

    @Test
    public void testGetTurn(){
        assertEquals(game.getTurn(), 0);
    }

    @Test
    public void testIncrmentTurn(){
        game.incrementTurn();
        assertEquals(game.getTurn(), 1);
    }

    @Test
    public void testIsGameOver(){
        assertFalse(game.isGameOver());
    }

    @Test
    public void testSetGameOver(){
        game.setGameOver();
        assertTrue(game.isGameOver());
    }

    @Test
    public void testGetObjCard(){
        assertEquals(game.getObjCards(), obj);
    }

    @Test
    public void testGetPlayer(){
        assertEquals(game.getPlayers(), players);
    }

    @Test
    public void testRemovePlayer(){
        game.removePlayer("bastien");
        //System.out.println(game.getPlayers().get(0).getSessionId());
        assertFalse(game.getPlayers().contains(user));

    }

    @Test
    public void testGetPlayerById(){
        assertEquals(game.getPlayerById("bastien"), user);
        assertEquals(game.getPlayerById("tristan"), null);
    }

    @Test
    public void testGetTerrCards(){
        assertEquals(game.getTerrCards(), terr);
    }

    @Test
    public void testAddCardTerr(){
        CarteTerr carteTerr;
        carteTerr = new CarteTerr();

        game.addTerrCard(carteTerr);
        assertTrue(game.getTerrCards().contains(carteTerr));
    }




}