package fr.alma.risk;

import fr.alma.risk.entity.Continent;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertTrue;

public class ContinentTest {
    private Continent continent;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        continent = Mockito.mock(Continent.class);
    }

    @Test
    public void testGetId(){
        Mockito.when(continent.getId()).thenReturn(2,6,3);

        assertTrue(continent.getId() == 2);
        assertTrue(continent.getId() == 6);
        assertTrue(continent.getId() == 3);
    }

    @Test
    public void testGetName(){
        Mockito.when(continent.getName()).thenReturn("Bresil", "Chine", "Congo");

        assertTrue(continent.getName() == "Bresil");
        assertTrue(continent.getName() == "Chine");
        assertTrue(continent.getName() == "Congo");
    }
}