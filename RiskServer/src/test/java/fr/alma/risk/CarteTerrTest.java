package fr.alma.risk;

import fr.alma.risk.entity.CarteTerr;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertTrue;


/*essaie de faire un mock du repository */
public class CarteTerrTest {
    private CarteTerr carteTerr;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        carteTerr = Mockito.mock(CarteTerr.class);
    }

    @Test
    public void testGetIdTerri(){
        Mockito.when(carteTerr.getIdTerri()).thenReturn(1,40);

        assertTrue(carteTerr.getIdTerri() == 1);
        assertTrue(carteTerr.getIdTerri() == 40);
    }

    @Test
    public void testGetArmee(){
        Mockito.when(carteTerr.getArme()).thenReturn("fantassin", "canon" , "cavalier");

        assertTrue(carteTerr.getArme() == "fantassin");
        assertTrue(carteTerr.getArme() == "canon");
        assertTrue(carteTerr.getArme() == "cavalier");
    }

}