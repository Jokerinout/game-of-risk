var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('/endpoint');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/user/queue/reply', function (greeting) {
            showGreeting(JSON.parse(greeting.body).name);
        });
    });
}

function init() {
    stompClient.send("/connect", {});
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendArmy() {
	var message = { idTerr: $("#territoire").val(), nb: $("#nb").val()};
	stompClient.send("/setupArmy", {},JSON.stringify(message));
}

function changeCard() {
	var message = { arme1: $("#territoire").val(), arme2: $("#nb").val(), arme3: $("#arme").val()};
	stompClient.send("/changeTerrCard", {},JSON.stringify(message));
}

function showGreeting(message) {
    $("#greetings").prepend("<tr><td>" + message + "</td></tr>");
}

function passTurn(){
    stompClient.send("/passTurn", {});
}

function ready(){
    stompClient.send("/ready", {});
}

function attack(){
   var message = { idDep: $("#territoire").val(), idCible: $("#nb").val(), nb: $("#arme").val()};
	stompClient.send("/attackTerritory", {},JSON.stringify(message));
}

function move(){
   var message = { idDep: $("#territoire").val(), idCible: $("#nb").val(), nb: $("#arme").val()};
	stompClient.send("/moveArmies", {},JSON.stringify(message));
}

function def(){
   var message = { terri: $("#territoire").val(), nb: $("#nb").val()};
	stompClient.send("/getDef", {},JSON.stringify(message));
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#send" ).click(function() { init(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#sendArmy" ).click(function() { sendArmy(); });
    $( "#sendCard" ).click(function() { changeCard(); });
    $( "#passTurn" ).click(function() { passTurn(); });
    $( "#ready" ).click(function() { ready(); });
    $( "#attack" ).click(function() { attack(); });
    $( "#move" ).click(function() { move(); });
    $( "#def" ).click(function() { def(); });
});
