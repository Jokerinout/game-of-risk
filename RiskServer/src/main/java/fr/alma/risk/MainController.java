package fr.alma.risk;

import fr.alma.risk.message.HelloMessage;
import fr.alma.risk.repository.*;
import fr.alma.risk.entity.*;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

@Controller    // This means that this class is a Controller
@RequestMapping(path="/app") // This means URL's start with /demo (after Application path)
public class MainController {

	@Autowired
	private GameApplication application;

	private static final Logger LOGGER = Logger.getLogger(MainController.class.getName());

	private List<User> waitingUsers = new ArrayList<>();

	@Autowired
	private SimpMessagingTemplate simpMessagingTemplate;

	private static List<String> tabColor = new ArrayList<String>(Arrays.asList("jaune","rouge","bleues","noires","violettes","vertes"));

	/**
	 * {@inheritDoc}
	 */

	@MessageMapping("/connect")
	@SendTo("/user/queue/reply")
	public void connectGame(@Header("simpSessionId") String sessionId){
		boolean b = false;
		LOGGER.info("Received a connection from the user " + sessionId);
		User u = new User(tabColor.get(waitingUsers.size()),sessionId);
		waitingUsers.add(u);
		for(int i=0;i<waitingUsers.size();i++){
			simpMessagingTemplate.convertAndSend(
					"/queue/reply-user" + waitingUsers.get(i).getSessionId(),
					new HelloMessage("Les armées " + waitingUsers.get(waitingUsers.size()-1).getColor() + " entrent en jeu"));
		}
		while(!b){
			try {
				Thread.sleep(100); // for 100 FPS
			} catch (InterruptedException ignore) {}
			if(waitingUsers.size() == 6){
				b=true;
			}else{
				if(waitingUsers.size() >= 2 && checkPlayers()){
					b=true;
				}
			}
		}
		allPlayersAreReady();
		LOGGER.info("Starting Game");
		List<User> temp = new ArrayList<>();
		for(int i=0; i<waitingUsers.size();i++){
			temp.add(waitingUsers.get(i));
		}
		waitingUsers.clear();
		application.createGame(temp);
	}

	/**
	 * {@inheritDoc}
	 */

	@MessageMapping("/attackTerritory")
	@SendTo("user/queue/reply_attack")
	public void attack(@Header("simpSessionId") String sessionId, String message){
		JsonElement jelement = new JsonParser().parse(message);
		JsonObject jobject = jelement.getAsJsonObject();
		String id_dep_Json = jobject.get("idDep").getAsString();
		String id_cible_Json = jobject.get("idCible").getAsString();
		int NBJson = jobject.get("nb").getAsInt();
		LOGGER.info("the user " + sessionId + " attack ");
		application.attackThisTerritory(sessionId,id_dep_Json,id_cible_Json,NBJson);
	}

	/**
	 * {@inheritDoc}
	 */

	@MessageMapping("/moveArmies")
	@SendTo("user/queue/reply_movement")
	public void moveArmies(@Header("simpSessionId") String sessionId, String message){
		JsonElement jelement = new JsonParser().parse(message);
		JsonObject jobject = jelement.getAsJsonObject();
		String id_dep_Json = jobject.get("idDep").getAsString();
		String id_cibe_Json = jobject.get("idCible").getAsString();
		int NBJson = jobject.get("nb").getAsInt();
		LOGGER.info("the user " + sessionId + " move his armies");
		application.moveArmyToTerritory(sessionId,id_dep_Json,id_cibe_Json,NBJson);
	}

	/**
	 * {@inheritDoc}
	 */

	@MessageMapping("/getDef")
	@SendTo("user/queue/reply_def")
	public void getDef(@Header("simpSessionId") String sessionId, String message){
		JsonElement jelement = new JsonParser().parse(message);
		JsonObject jobject = jelement.getAsJsonObject();
		String id_Json = jobject.get("terri").getAsString();
		int nb_Json = jobject.get("nb").getAsInt();
		LOGGER.info("the user " + sessionId + " se défend ");
		application.getDef(sessionId,id_Json,nb_Json);
	}

	/**
	 * {@inheritDoc}
	 */

	@MessageMapping("/setupArmy")
	@SendTo("/user/queue/reply_setupArmy")
	public void setupArmy(@Header("simpSessionId") String sessionId, String message){
		JsonElement jelement = new JsonParser().parse(message);
		JsonObject jobject = jelement.getAsJsonObject();
		int terr_id_Json = jobject.get("idTerr").getAsInt();
		int NBJson = jobject.get("nb").getAsInt();
		LOGGER.info("the user " + sessionId + " change army on " + terr_id_Json);
		application.changeArmyTerritory(sessionId,terr_id_Json,NBJson);
	}

	/**
	 * {@inheritDoc}
	 */

	@MessageMapping("/changeTerrCard")
	@SendTo("/user/queue/reply_changeTerrCard")
	public void changingTerrCard(@Header("simpSessionId") String sessionId,String message){
		JsonElement jelement = new JsonParser().parse(message);
		JsonObject jobject = jelement.getAsJsonObject();
		int arme1_Json = jobject.get("arme1").getAsInt();
		int arme2_Json = jobject.get("arme2").getAsInt();
		int arme3_Json = jobject.get("arme3").getAsInt();
		LOGGER.info("the user " + sessionId + " change territories card ");
		application.changeCardTerr(sessionId,arme1_Json,arme2_Json,arme3_Json);
	}

	/**
	 * {@inheritDoc}
	 */

	@MessageMapping("/passTurn")
	public void passTurn(@Header("simpSessionId") String sessionId){
		application.passTurn(sessionId);
	}

	/**
	 * {@inheritDoc}
	 */

	@MessageMapping("/ready")
	@SendTo("/user/queue/reply")
	public void getReady(@Header("simpSessionId") String sessionId){
		for(int i=0;i<waitingUsers.size();i++){
			if(waitingUsers.get(i).getSessionId().equals(sessionId)){
				waitingUsers.get(i).changePassTurn();
				simpMessagingTemplate.convertAndSend(
						"/queue/reply-user" + waitingUsers.get(i).getSessionId(),
						new HelloMessage("vous êtes prêt, en attente des autres joueurs.."));
			}
		}
	}

	private boolean checkPlayers(){
		for(int i=0;i<waitingUsers.size();i++){
			if(!waitingUsers.get(i).getPassTurn()){
				return false;
			}
		}
		return true;
	}

	private void allPlayersAreReady(){
		for(int i=0;i<waitingUsers.size();i++){
			waitingUsers.get(i).changePassTurn();
		}
	}
}
