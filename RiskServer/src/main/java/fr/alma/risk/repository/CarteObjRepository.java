package fr.alma.risk.repository;

import fr.alma.risk.entity.CarteObj;
import org.springframework.data.jpa.repository.JpaRepository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface CarteObjRepository extends JpaRepository<CarteObj, Integer> {

}