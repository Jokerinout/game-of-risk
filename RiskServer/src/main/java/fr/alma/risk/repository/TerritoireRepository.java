package fr.alma.risk.repository;

import fr.alma.risk.entity.Territoire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface TerritoireRepository extends JpaRepository<Territoire, Integer> {

    /**
     * find a territory in the database according the name of a territory
     *
     * @param name the name of a territory
     * @return a territory
     */

    @Query("select t from Territoire t where t.territoire_name = ?1")
    public Territoire findByTerritoire_name (String name);

    /**
     * find a territory in the database according the id of a territory
     *
     * @param id the id of a territory
     * @return a territory
     */

    @Query("select t from Territoire t where t.idT = ?1")
    public Territoire findTerrById (int id);

    /**
     * find all territories from a continent in the database according the name of a continent
     *
     * @param nameCont the name of a continent
     * @return a list of territory
     */

    @Query("select t from Territoire t where t.continent.continent_name = ?1")
    public List<Territoire> findAllTerrFromCont(String nameCont);
}
