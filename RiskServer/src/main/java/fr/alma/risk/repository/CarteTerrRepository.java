package fr.alma.risk.repository;

import fr.alma.risk.entity.CarteTerr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface CarteTerrRepository extends JpaRepository<CarteTerr, Integer> {

    /**
     * find a territory card in the database according the id of a territory
     *
     * @param id the id of a territory
     * @return a territory card
     */

    @Query("select t from CarteTerr t where t.idTerri = ?1")
    public CarteTerr findCardTerrByIdTerr (String id);

    /**
     * find a territory card in the database according the id of a territory card
     *
     * @param id the id of a territory card
     * @return a territory card
     */

    @Query("select t from CarteTerr t where t.idCarteTer = ?1")
    public CarteTerr findCardTerrById (int id);

}