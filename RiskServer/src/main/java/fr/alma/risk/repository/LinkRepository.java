package fr.alma.risk.repository;;

import fr.alma.risk.entity.Link;
import fr.alma.risk.entity.Territoire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface LinkRepository extends JpaRepository<Link, Integer> {

    /**
     * find all territories in the database who are linked with the territory in parameter
     *
     * @param name of the territory
     * @return a list of territories
     */

    @Query("select l.territoire_fin from Link l where l.territoire_deb.territoire_name = ?1")
    public List<Territoire> findAllLinkTerritoire_name (String name);
}