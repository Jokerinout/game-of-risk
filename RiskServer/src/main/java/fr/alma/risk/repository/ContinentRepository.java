package fr.alma.risk.repository;

import fr.alma.risk.entity.Continent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface ContinentRepository extends JpaRepository<Continent, Integer> {

    /**
     * find a continent in the database according the name of the continent
     *
     * @param name the id of a territory
     * @return a continent
     */

    @Query("select c from Continent c where c.continent_name = ?1")
    public Continent findByContinent_name (String name);
}
