package fr.alma.risk;

import fr.alma.risk.message.DefenseMessage;
import fr.alma.risk.message.HelloMessage;
import fr.alma.risk.message.PlayerMessage;
import fr.alma.risk.message.TerritoryMessage;
import fr.alma.risk.repository.*;
import fr.alma.risk.entity.*;

import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SpringBootApplication //(exclude = {DataSourceAutoConfiguration.class})
//@ComponentScan(basePackageClasses = fr.alma.risk.Repository.ContinentRepository.class)

public class GameApplication {

    @Autowired
    private ContinentRepository ContinentRepository;

    @Autowired
    private TerritoireRepository TerritoireRepository;

    @Autowired
    private LinkRepository LinkRepository;

    @Autowired
    private CarteTerrRepository CarteTerrRepository;

    @Autowired
    private CarteObjRepository CarteObjRepository;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    private List<User> players = new ArrayList<>();

    private int rightToChangeCards = 0;

    private int rightToPlay = 0;

    private int nbDesDef = 0;

    private List<CarteTerr> cardEchange = new ArrayList<>();

    public static void main(String[] args) {
        SpringApplication.run(GameApplication.class, args);
    }

    /**
     * allows to create a new game
     */

    public void createGame(List<User> joueurs){
        players = joueurs;
        Game game = new Game(joueurs,CarteObjRepository.findAll(),CarteTerrRepository.findAll());
        for(int i=0;i<game.getPlayers().size();i++){
            game.getPlayers().get(i).setGame(game);
        }
        // preparation of the game (shuffle cards and territories, attribute all necessary things to players)
        setupGame(game);
        // send to players all their games
        for(int j=0;j<joueurs.size();j++) {
            List<List<Territoire>> temp = new ArrayList<>();
            for(int k=0;k<joueurs.get(j).getTabTerri().size();k++){
                temp.add(LinkRepository.findAllLinkTerritoire_name(joueurs.get(j).getTabTerri().get(k).getName()));
            }
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply_myterritories-user" + joueurs.get(j).getSessionId(),
                    new PlayerMessage(joueurs.get(j).getTabTerri(),temp,joueurs.get(j).getTabCont(),joueurs.get(j).getMyCardsTerr(),joueurs.get(j).getColor(),joueurs.get(j).getMyCardObj().getDescription(),joueurs.get(j).getNbArmeeTotal(),joueurs.get(j).getArmeeAplacer()));
        }
        // place the armies
        setupArmies(game);
        for(int z=0;z<joueurs.size();z++) {
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + joueurs.get(z).getSessionId(),
                    new HelloMessage("toute les armées sont placées !"));
        }
        game.incrementTurn();
        boolean win = game.isGameOver();
        while(!win || game.getPlayers().size() != 1){
            initiateTurn(game.getCurrentPlayer(), game.getTurn());
            setupArmies(game);
            putEchangeCards(game);
            this.rightToChangeCards = 0;
            playTurn(game.getCurrentPlayer());
            if(playerWin(game.getCurrentPlayer(), game.getCurrentPlayer().getColor())){
                win = true;
            }
            game.getCurrentPlayer().changePassTurn();
            this.rightToPlay=0;
            game.incrementTurn();
        }
        for(int z=0; z<game.getPlayers().size();z++){
            if(game.getWinner().equals(game.getPlayers().get(z).getSessionId())){
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + game.getPlayers().get(z).getSessionId(),
                        new HelloMessage("vous avez GAGNE :) "));
            }else{
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + game.getPlayers().get(z).getSessionId(),
                        new HelloMessage("vous avez PERDU :'("));
            }
        }
    }

    /**
     * the server prepare the game
     */

    public void setupGame(Game game){
        game.shuffleObjectif();
        game.shuffleTerritoire();
        int compt = 0;
        for(int i=0;i<game.getPlayers().size();i++){
            game.getPlayers().get(i).setCardObj(game.pullaObjCard());
            game.getPlayers().get(i).changeArmy(40-(5*(game.getPlayers().size()-2)));
            game.getPlayers().get(i).setArmeeAplacer(40-(5*(game.getPlayers().size()-2)));
        }
        for(int z=0;z<game.getTerrCards().size();z++){
            if(compt == game.getPlayers().size()){
                compt = 0;
            }
            game.getPlayers().get(compt).addTerr(TerritoireRepository.findTerrById(game.getTerrCards().get(z).getIdTerri()));
            game.getPlayers().get(compt).incrementArmeeAplacer(-1);
            game.getPlayers().get(compt).getTabTerri().get(game.getPlayers().get(compt).getTabTerri().size()-1).changeArmy(1);
            game.getPlayers().get(compt).getTabTerri().get(game.getPlayers().get(compt).getTabTerri().size()-1).setUser(game.getPlayers().get(compt));
            compt ++;
        }
        for(int k=0;k<game.getPlayers().size();k++){
            for(int f=0;f<game.getPlayers().get(k).getTabTerri().size();f++){
                for(int n=0;n<game.getPlayers().size();n++){
                    sendTerritory(game.getPlayers().get(n).getSessionId(),game.getPlayers().get(k).getTabTerri().get(f));
                }
            }
        }
    }

    /**
     * check the continents owned by a user
     *
     * @param u a User
     *
     * @return a List of Continents owned by the user
     */

    private List<Continent> checkCont(User u){
        boolean b = true;
        List<Continent> res = new ArrayList<>();
        for(int i=0;i<ContinentRepository.count();i++){
            for(int j=0;j<TerritoireRepository.findAllTerrFromCont(ContinentRepository.findAll().get(i).getName()).size();j++){
                if(!u.containsTerr(TerritoireRepository.findAllTerrFromCont(ContinentRepository.findAll().get(i).getName()).get(j).getName()) || u.containsCont(ContinentRepository.findAll().get(i).getId())){
                    b = false;
                }
            }
            if(b == true){
                res.add(ContinentRepository.findAll().get(i));
            }
            b=true;
        }
        return res;
    }

    /**
     * give a User in terms of one of his territory id
     *
     * @param terrID the id of the territory own by the user
     *
     * @return a User
     */

    private User getPlayerByTerrId(int terrID){
        for (int i=0;i<players.size();i++){
            if(players.get(i).findTerr(terrID) != null){
                return players.get(i);
            }
        }
        return null;
    }

    /**
     * give a User in terms of his id
     *
     * @param id the id of the user
     *
     * @return a User
     */

    private User getPlayerById(String id){
        for (int i=0;i<players.size();i++){
            if(players.get(i).getSessionId() == id){
                return players.get(i);
            }
        }
        return null;
    }

    /**
     * check if an id is in the list of territories, this is used to check if one territory is linked with an other
     *
     * @param ter the list of territories
     * @param idTer the id of one territory
     *
     * @return a boolean
     */

    private boolean terrAdjacent(List<Territoire> ter, String idTer){
        for(int i=0; i<ter.size();i++){
            if(ter.get(i).getId().equals(Integer.parseInt(idTer))){
                return true;
            }
        }
        return false;
    }

    /**
     * use to get the number of dice a player will use to defend one of his territory
     */

    public void getDef(String sessionId, String terrId,int nb){
        if(nb >= 1 && nb <= 2 && getPlayerById(sessionId).findTerr(Integer.parseInt(terrId)).getNbArmee()-nb >= 0){
            if(getPlayerByTerrId(Integer.parseInt(terrId)).getSessionId().equals(sessionId)){
                this.nbDesDef = nb;
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + sessionId,
                        new HelloMessage("vous défendez avec " + nb + " dés"));
            }else{
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + sessionId,
                        new HelloMessage("vous n'avez rien à défendre"));
            }
        }else{
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + sessionId,
                    new HelloMessage("vous ne pouvez défendre avec ce nombre de dés (max 2 dés)"));
        }
    }

    /**
     * search the max of a list in parameter, return it
     *
     * @param list the list on integer
     *
     * @return an integer
     */

    private int searchMaxList(List<Integer> list){
        int max = 0;
        for(int i=0;i<list.size();i++){
            if(list.get(i) > max){
                max = list.get(i);
            }
        }
        return max;
    }

    /**
     * remove the max of a list in parameter
     *
     * @param list the list of integer
     */

    private void suppMaxList(List<Integer> list){
        int max = 0;
        int ind = 0;
        for(int i=0;i<list.size();i++){
            if(list.get(i) > max){
                ind = i;
            }
        }
        list.remove(ind);
    }

    /**
     * using to move army from a territory, to another
     *
     */

    public void moveArmyToTerritory(String sessionId,String idTerrDep, String idTerrCible, int nb){
        if(getPlayerById(sessionId).containsTerrById(Integer.parseInt(idTerrDep)) && getPlayerById(sessionId).containsTerrById(Integer.parseInt(idTerrCible)) && getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrDep)).getNbArmee()-nb >=1 && terrAdjacent(LinkRepository.findAllLinkTerritoire_name(getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrDep)).getName()),idTerrCible)){
            getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrDep)).changeArmy(-nb);
            getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrCible)).changeArmy(nb);
            List<List<Territoire>> temp = new ArrayList<>();
            for(int n=0;n<players.size();n++){
                sendTerritory(players.get(n).getSessionId(),getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrCible)));
                sendTerritory(players.get(n).getSessionId(),getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrDep)));
            }
            for(int f=0;f<getPlayerById(sessionId).getTabTerri().size();f++){
                temp.add(LinkRepository.findAllLinkTerritoire_name(getPlayerById(sessionId).getTabTerri().get(f).getName()));
            }
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply_myterritories-user" + sessionId,
                    new PlayerMessage(getPlayerById(sessionId).getTabTerri(),temp,getPlayerById(sessionId).getTabCont(),getPlayerById(sessionId).getMyCardsTerr(),getPlayerById(sessionId).getColor(),getPlayerById(sessionId).getMyCardObj().getDescription(),getPlayerById(sessionId).getNbArmeeTotal(),getPlayerById(sessionId).getArmeeAplacer()));
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + sessionId,
                    new HelloMessage("vous avez déplacé " + nb + " armées depuis le territoire " + getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrDep)).getName() + " vers " + getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrCible)).getName()));
        }else{
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + sessionId,
                    new HelloMessage("vous ne pouvez déplacer ces armées"));
        }
    }

    /**
     * using to attack a territory from one to another with a number of armies
     *
     */

    public void attackThisTerritory(String sessionId, String idTerrDep, String idTerrCible, int nb){
        List<Integer> desAttack = new ArrayList<>();
        List<Integer> desDef = new ArrayList<>();
        int armeePerduAttack = 0;
        int armeePerduDef = 0;
        int desCourantAttack = 0;
        int desCourantDef = 0;
        String idDefense = getPlayerByTerrId(Integer.parseInt(idTerrCible)).getSessionId();
        // if the player can attack
        if(getPlayerById(sessionId).getGame().getCurrentPlayer().getSessionId().equals(sessionId) && rightToPlay==1 && terrAdjacent(LinkRepository.findAllLinkTerritoire_name(getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrDep)).getName()),idTerrCible) && nb >= 1 && getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrDep)).getNbArmee()-nb >=1){
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + sessionId,
                    new HelloMessage("vous attaquez le territoire " + TerritoireRepository.findTerrById(Integer.parseInt(idTerrCible)).getName() + " depuis le territoire " + getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrDep)).getName()));
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + sessionId,
                    new HelloMessage("le défenseur possède " + getPlayerByTerrId(Integer.parseInt(idTerrCible)).findTerr(Integer.parseInt(idTerrCible)).getNbArmee() + " armées sur ce Territoire"));
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + getPlayerByTerrId(Integer.parseInt(idTerrCible)).getSessionId(),
                    new HelloMessage("les armées " + getPlayerById(sessionId).getColor() + " veulent attaquer le territoire " + TerritoireRepository.findTerrById(Integer.parseInt(idTerrCible)).getName()));
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply_askingDefence-user" + getPlayerByTerrId(Integer.parseInt(idTerrCible)).getSessionId(),
                    new DefenseMessage(TerritoireRepository.findTerrById(Integer.parseInt(idTerrCible)).getName()));
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + getPlayerByTerrId(Integer.parseInt(idTerrCible)).getSessionId(),
                    new HelloMessage("vous avez actuellement " + getPlayerByTerrId(Integer.parseInt(idTerrCible)).findTerr(Integer.parseInt(idTerrCible)).getNbArmee() + " armées sur ce territoire, avec combien de dés souhaitez-vous défendre ?"));
            while(nbDesDef == 0){
                try {
                    Thread.sleep(10); // for 100 FPS
                } catch (InterruptedException ignore) {}
            }
            if(nb <= 3){
                for(int i=0; i<nb;i++){
                    desAttack.add((int)(Math.random() * ((7 - 1) )+1));
                }
            }else{
                for(int i=0; i<3;i++){
                    desAttack.add((int)(Math.random() * ((7 - 1) )+1));
                }
            }
            for(int j=0;j<this.nbDesDef;j++){
                desDef.add((int)(Math.random() * ((7 - 1) )+1));
            }
            // compare the dice
            if(desAttack.size() >= desDef.size()){
                int taille = desDef.size();
                for(int k=0;k<taille;k++){
                    desCourantAttack = searchMaxList(desAttack);
                    desCourantDef = searchMaxList(desDef);
                    if(desCourantAttack > desCourantDef){
                        getPlayerByTerrId(Integer.parseInt(idTerrCible)).findTerr(Integer.parseInt(idTerrCible)).changeArmy(-1);
                        getPlayerByTerrId(Integer.parseInt(idTerrCible)).changeArmy(-1);
                        armeePerduDef++;
                    }else{
                        getPlayerByTerrId(Integer.parseInt(idTerrDep)).findTerr(Integer.parseInt(idTerrDep)).changeArmy(-1);
                        getPlayerById(sessionId).changeArmy(-1);
                        armeePerduAttack++;
                    };
                    suppMaxList(desAttack);
                    suppMaxList(desDef);
                }
            }else{
                int taille = desAttack.size();
                for(int k=0;k<taille;k++){
                    desCourantAttack = searchMaxList(desAttack);
                    desCourantDef = searchMaxList(desDef);
                    if(desCourantAttack > desCourantDef){
                        getPlayerByTerrId(Integer.parseInt(idTerrCible)).findTerr(Integer.parseInt(idTerrCible)).changeArmy(-1);
                        getPlayerByTerrId(Integer.parseInt(idTerrCible)).changeArmy(-1);
                        armeePerduDef++;
                    }else{
                        getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrDep)).changeArmy(-1);
                        getPlayerById(sessionId).changeArmy(-1);
                        armeePerduAttack++;
                    }
                }
                suppMaxList(desAttack);
                suppMaxList(desDef);
            }
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + sessionId,
                    new HelloMessage("vous avez perdus " + armeePerduAttack + " armées"));
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + getPlayerByTerrId(Integer.parseInt(idTerrCible)).getSessionId(),
                    new HelloMessage("vous avez perdus " + armeePerduDef + " armées"));
            if(getPlayerByTerrId(Integer.parseInt(idTerrCible)).findTerr(Integer.parseInt(idTerrCible)).getNbArmee() == 0){
                // if the territory is won
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + sessionId,
                        new HelloMessage("vous avez conquis le territoire " + TerritoireRepository.findTerrById(Integer.parseInt(idTerrCible)).getName()));
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + getPlayerByTerrId(Integer.parseInt(idTerrCible)).getSessionId(),
                        new HelloMessage("vous avez perdu le territoire " + TerritoireRepository.findTerrById(Integer.parseInt(idTerrCible)).getName()));
                idDefense = getPlayerByTerrId(Integer.parseInt(idTerrCible)).getSessionId();
                if(getPlayerById(idDefense).containsContByContName(getPlayerById(idDefense).findTerr(Integer.parseInt(idTerrCible)).getContinent())){
                    getPlayerById(idDefense).removeContByName(getPlayerById(idDefense).findTerr(Integer.parseInt(idTerrCible)).getContinent());
                }
                getPlayerByTerrId(Integer.parseInt(idTerrCible)).findTerr(Integer.parseInt(idTerrCible)).setUser(getPlayerById(sessionId));
                getPlayerById(sessionId).addTerr(getPlayerByTerrId(Integer.parseInt(idTerrCible)).removeTerrByTerrId(Integer.parseInt(idTerrCible)));
                getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrCible)).setNbArmee(nb-armeePerduAttack);
                getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrDep)).changeArmy(-(nb-armeePerduAttack));
                getPlayerById(sessionId).addCardTerr(getPlayerById(sessionId).getGame().pullaTerrCard());
                for(int m=0;m<checkCont(getPlayerById(sessionId)).size();m++){
                    getPlayerById(sessionId).addCont(checkCont(getPlayerById(sessionId)).get(m));
                }
                for(int n=0;n<players.size();n++){
                    sendTerritory(players.get(n).getSessionId(),getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrCible)));
                    sendTerritory(players.get(n).getSessionId(),getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrDep)));
                }
                if(getPlayerById(idDefense).getTabTerri().size()==0){
                    getPlayerById(idDefense).elimine();
                    simpMessagingTemplate.convertAndSend(
                            "/queue/reply-user" + getPlayerById(idDefense),
                            new HelloMessage("vous êtes éliminé :'( "));
                    getPlayerById(sessionId).getGame().removePlayer(idDefense);
                }
                if(playerWin(getPlayerById(sessionId),getPlayerById(sessionId).getColor())) {
                    getPlayerById(sessionId).getGame().setGameOver();
                    getPlayerById(sessionId).getGame().setWinner(sessionId);
                }
            }else{
                for(int n=0;n<players.size();n++){
                    sendTerritory(players.get(n).getSessionId(),getPlayerById(sessionId).findTerr(Integer.parseInt(idTerrDep)));
                }
            }
            List<List<Territoire>> temp = new ArrayList<>();
            List<List<Territoire>> temp2 = new ArrayList<>();
            for(int f=0;f<getPlayerById(idDefense).getTabTerri().size();f++){
                temp.add(LinkRepository.findAllLinkTerritoire_name(getPlayerById(idDefense).getTabTerri().get(f).getName()));
            }
            for(int l=0;l<getPlayerById(sessionId).getTabTerri().size();l++){
                temp2.add(LinkRepository.findAllLinkTerritoire_name(getPlayerById(sessionId).getTabTerri().get(l).getName()));
            }
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply_myterritories-user" + idDefense,
                    new PlayerMessage(getPlayerById(idDefense).getTabTerri(),temp,getPlayerById(idDefense).getTabCont(),getPlayerById(idDefense).getMyCardsTerr(),getPlayerById(idDefense).getColor(),getPlayerById(idDefense).getMyCardObj().getDescription(),getPlayerById(idDefense).getNbArmeeTotal(),getPlayerById(idDefense).getArmeeAplacer()));
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply_myterritories-user" + sessionId,
                    new PlayerMessage(getPlayerById(sessionId).getTabTerri(),temp2,getPlayerById(sessionId).getTabCont(),getPlayerById(sessionId).getMyCardsTerr(),getPlayerById(sessionId).getColor(),getPlayerById(sessionId).getMyCardObj().getDescription(),getPlayerById(sessionId).getNbArmeeTotal(),getPlayerById(sessionId).getArmeeAplacer()));
            this.nbDesDef = 0;
        }else{
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + sessionId,
                    new HelloMessage("vous ne pouvez pas attaquer ce territoire"));
        }
    }

    /**
     * using to change the number of armies from a territory
     *
     */

    public void changeArmyTerritory(String sessionId,int idTerri,int nb){
        User u = getPlayerById(sessionId);
        if(u.getArmeeAplacer() == 0){
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + sessionId,
                    new HelloMessage("vous n'avez plus d'armees à placer "));
        }else{
            if(u.getArmeeAplacer() - nb >= 0 && u.findTerr(idTerri) != null){
                u.findTerr(idTerri).changeArmy(nb);
                u.incrementArmeeAplacer(-nb);
                for(int n=0;n<players.size();n++){
                    sendTerritory(players.get(n).getSessionId(),getPlayerByTerrId(idTerri).findTerr(idTerri));
                }
                List<List<Territoire>> temp = new ArrayList<>();
                for(int l=0;l<u.getTabTerri().size();l++){
                    temp.add(LinkRepository.findAllLinkTerritoire_name(u.getTabTerri().get(l).getName()));
                }
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply_myterritories-user" + u.getSessionId(),
                        new PlayerMessage(u.getTabTerri(),temp,u.getTabCont(),u.getMyCardsTerr(),u.getColor(),u.getMyCardObj().getDescription(),u.getNbArmeeTotal(),u.getArmeeAplacer()));
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + sessionId,
                        new HelloMessage("Vous avez placé " + nb + " armées sur le territoire " + u.findTerr(idTerri).getName() + ", il vous reste " + getPlayerById(sessionId).getArmeeAplacer() + " armées à placer"));
            }else{
                if(u.getArmeeAplacer() - nb < 0 && u.findTerr(idTerri) != null){
                    simpMessagingTemplate.convertAndSend(
                            "/queue/reply-user" + sessionId,
                            new HelloMessage("vous n'avez pas assez d'armées à placer, il vous reste " + getPlayerById(sessionId).getArmeeAplacer() + " armées à placer"));
                }
            }
        }
    }

    /**
     * using at the begining of the turn to put armies on a territory
     *
     */

    public void setupArmies(Game game){
        boolean b =false;
        if(game.getTurn() == 0){
            for(int i=0;i<game.getPlayers().size();i++){
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + game.getPlayers().get(i).getSessionId(),
                        new HelloMessage("Vous allez pouvoir attribuer vos " + game.getPlayers().get(i).getArmeeAplacer() + " armées restantes"));
            }
        }else{
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + game.getCurrentPlayer().getSessionId(),
                    new HelloMessage("Vous allez pouvoir attribuer vos " + game.getCurrentPlayer().getArmeeAplacer() + " armées restantes"));
            List<List<Territoire>> temp = new ArrayList<>();
            for(int l=0;l<game.getCurrentPlayer().getTabTerri().size();l++){
                temp.add(LinkRepository.findAllLinkTerritoire_name(game.getCurrentPlayer().getTabTerri().get(l).getName()));
            }
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply_myterritories-user" + game.getCurrentPlayer().getSessionId(),
                    new PlayerMessage(game.getCurrentPlayer().getTabTerri(),temp,game.getCurrentPlayer().getTabCont(),game.getCurrentPlayer().getMyCardsTerr(),game.getCurrentPlayer().getColor(),game.getCurrentPlayer().getMyCardObj().getDescription(),game.getCurrentPlayer().getNbArmeeTotal(),game.getCurrentPlayer().getArmeeAplacer()));

        }
        while(!b){
            b = checkArmy(game);
        }
    }

    /**
     * check if all the players have placed their armies
     *
     * @param game a game
     *
     * @return boolean
     */

    private boolean checkArmy(Game game){
        for(int i=0;i<game.getPlayers().size();i++) {
            if (game.getPlayers().get(i).getArmeeAplacer() != 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * using to move army from a territory, to another
     *
     */

    public void sendTerritory(String id, Territoire terr){
        TerritoryMessage territory = new TerritoryMessage(terr.getId(),terr.getName(),terr.getNbArmee(),terr.getUser().getColor());
        simpMessagingTemplate.convertAndSend(
                "/queue/reply_sendterritory-user" + id, territory);
    }

    /**
     * give all armies to a User in terms of his territories, his continent, his territories card
     *
     */

    public void initiateTurn(User u, int turn){
        for(int j=0; j<players.size();j++){
            if(players.get(j).getSessionId().equals(u.getSessionId())){
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + u.getSessionId(),
                        new HelloMessage("C'est votre tour !"));
            }else{
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + players.get(j).getSessionId(),
                        new HelloMessage("en attente des armées " + u.getColor()));
            }
        }
        for(int k=0;k<checkCont(u).size();k++){
            u.addCont(checkCont(u).get(k));
        }
        List<List<Territoire>> temp = new ArrayList<>();
        for(int l=0;l<u.getTabTerri().size();l++){
            temp.add(LinkRepository.findAllLinkTerritoire_name(u.getTabTerri().get(l).getName()));
        }
        simpMessagingTemplate.convertAndSend(
                "/queue/reply_myterritories-user" + u.getSessionId(),
                new PlayerMessage(u.getTabTerri(),temp,u.getTabCont(),u.getMyCardsTerr(),u.getColor(),u.getMyCardObj().getDescription(),u.getNbArmeeTotal(),u.getArmeeAplacer()));
        float f = u.getTabTerri().size() / 3f;
        int army = 0;
        for(int i=0;i<u.getTabCont().size();i++){
            switch (u.getTabCont().get(i).getName()){
                case "Oceanie":
                case "Amerique du sud":
                    army += 2;
                    break;
                case "Afrique":
                    army += 3;
                    break;
                case "Europe":
                case "Amerique du nord":
                    army += 5;
                    break;
                case "Asie":
                    army += 7;
                    break;
            }
        }
        u.changeArmy((int) f + army);
        u.incrementArmeeAplacer((int) f + army);
        simpMessagingTemplate.convertAndSend(
                "/queue/reply-user" + u.getSessionId(),
                new HelloMessage("vous avez reçu " + ((int)f + army) + " armées"));
        if(u.getMyCardsTerr().size() == 0){
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + u.getSessionId(),
                    new HelloMessage("vous n'avez aucune carte territoire, pour le moment"));
        }else{
            if(u.getMyCardsTerr().size() >= 5){
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + u.getSessionId(),
                        new HelloMessage("vous avez plus de cinqs cartes territoires, vous devez en échanger."));
                while(u.getMyCardsTerr().size() >= 5){
                    try {
                        Thread.sleep(10); // for 100 FPS
                    } catch (InterruptedException ignore) {}
                }
            }
        }
        if(turn >= players.size()*3){
            this.rightToChangeCards = 1;
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + u.getSessionId(),
                    new HelloMessage("vous allez pouvoir echanger vos cartes territoires pour obtenir de nouvelles armées !"));
        }
    }

    /**
     * re put a card in the bunch of territory card and shuffle the package
     *
     * @param game a Game
     */

    private void putEchangeCards(Game game){
        for (int i=0;i<this.cardEchange.size();i++){
            game.addTerrCard(this.cardEchange.get(i));
        }
        game.shuffleTerritoire();
    }

    /**
     * using to change territory cards
     *
     */

    public void changeCardTerr(String sessionId,int idArme1,int idArme2,int idArme3){
        boolean b = false;
        User u = getPlayerById(sessionId);
        if(this.rightToChangeCards == 1 && u.getMyCardsTerr().size() >= 3 && u.searchCardTerr(idArme1) && u.searchCardTerr(idArme2) && u.searchCardTerr(idArme3)){
            if(CarteTerrRepository.findCardTerrById(idArme1).getArme().equals("fantassin") && CarteTerrRepository.findCardTerrById(idArme2).getArme().equals("fantassin") && CarteTerrRepository.findCardTerrById(idArme3).getArme().equals("fantassin") && u.occurencyCard("fantassin") >= 3){
                u.changeArmy(3);
                u.incrementArmeeAplacer(3);
                for(int i=0;i<3;i++){
                    cardEchange.add(u.removeCardTerr("fantassin"));
                }
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + u.getSessionId(),
                        new HelloMessage("vous avez échangé 3 cartes fantassin, et vous avez obtenu 3 armées"));
                b = true;
            }else{
                if(CarteTerrRepository.findCardTerrById(idArme1).getArme().equals("cavalier") && CarteTerrRepository.findCardTerrById(idArme2).getArme().equals("cavalier") && CarteTerrRepository.findCardTerrById(idArme3).getArme().equals("cavalier") && u.occurencyCard("cavalier") >= 3){
                    u.changeArmy(5);
                    u.incrementArmeeAplacer(5);
                    for(int i=0;i<3;i++){
                        cardEchange.add(u.removeCardTerr("cavalier"));
                    }
                    simpMessagingTemplate.convertAndSend(
                            "/queue/reply-user" + u.getSessionId(),
                            new HelloMessage("vous avez échangé 3 cartes cavalier, et vous avez obtenu 5 armées"));
                    b=true;
                }else {
                    if(CarteTerrRepository.findCardTerrById(idArme1).getArme().equals("canon") && CarteTerrRepository.findCardTerrById(idArme2).getArme().equals("canon") && CarteTerrRepository.findCardTerrById(idArme3).getArme().equals("canon") && u.occurencyCard("canon") >= 3){
                        u.changeArmy(8);
                        u.incrementArmeeAplacer(8);
                        for(int i=0;i<3;i++){
                            cardEchange.add(u.removeCardTerr("canon"));
                        }
                        simpMessagingTemplate.convertAndSend(
                                "/queue/reply-user" + u.getSessionId(),
                                new HelloMessage("vous avez échangé 3 cartes canon, et vous avez obtenu 8 armées"));
                        b=true;
                    }else{
                        if((CarteTerrRepository.findCardTerrById(idArme1).getArme().equals("cavalier") && CarteTerrRepository.findCardTerrById(idArme2).getArme().equals("fantassin") && CarteTerrRepository.findCardTerrById(idArme3).getArme().equals("canon")) || (CarteTerrRepository.findCardTerrById(idArme1).getArme().equals("cavalier") && CarteTerrRepository.findCardTerrById(idArme2).getArme().equals("canon") && CarteTerrRepository.findCardTerrById(idArme3).getArme().equals("fantassin")) || (CarteTerrRepository.findCardTerrById(idArme1).getArme().equals("canon") && CarteTerrRepository.findCardTerrById(idArme2).getArme().equals("fantassin") && CarteTerrRepository.findCardTerrById(idArme3).getArme().equals("cavalier")) || (CarteTerrRepository.findCardTerrById(idArme1).getArme().equals("canon") && CarteTerrRepository.findCardTerrById(idArme2).getArme().equals("cavalier") && CarteTerrRepository.findCardTerrById(idArme3).getArme().equals("fantassin")) || (CarteTerrRepository.findCardTerrById(idArme1).getArme().equals("fantassin") && CarteTerrRepository.findCardTerrById(idArme2).getArme().equals("canon") && CarteTerrRepository.findCardTerrById(idArme3).getArme().equals("cavalier")) || (CarteTerrRepository.findCardTerrById(idArme1).getArme().equals("fantassin") && CarteTerrRepository.findCardTerrById(idArme2).getArme().equals("cavalier") && CarteTerrRepository.findCardTerrById(idArme3).getArme().equals("canon")) && u.occurencyCard("fantassin") >= 1 && u.occurencyCard("canon") >= 1 && u.occurencyCard("cavalier") >= 1){
                            u.changeArmy(10);
                            u.incrementArmeeAplacer(10);
                            cardEchange.add(u.removeCardTerr("cavalier"));
                            cardEchange.add(u.removeCardTerr("canon"));
                            cardEchange.add(u.removeCardTerr("fantassin"));
                            simpMessagingTemplate.convertAndSend(
                                    "/queue/reply-user" + u.getSessionId(),
                                    new HelloMessage("vous avez échangé 1 carte fantassin, 1 canon et 1 cavalier, et vous avez obtenu 10 armées"));
                            b=true;
                        }else{
                            simpMessagingTemplate.convertAndSend(
                                    "/queue/reply-user" + u.getSessionId(),
                                    new HelloMessage("vos cartes ne correspondent à aucune combinaison possible"));
                        }
                    }
                }
            }
            if(b){
                if(u.containsTerr(TerritoireRepository.findTerrById(CarteTerrRepository.findCardTerrById(idArme1).getIdTerri()).getName())){
                    u.findTerr(TerritoireRepository.findTerrById(CarteTerrRepository.findCardTerrById(idArme1).getIdTerri()).getId()).changeArmy(2);
                    simpMessagingTemplate.convertAndSend(
                            "/queue/reply-user" + u.getSessionId(),
                            new HelloMessage("vous possédez le territoire " + TerritoireRepository.findTerrById(CarteTerrRepository.findCardTerrById(idArme1).getIdTerri()).getName() + " vous obtenez donc 2 armées supplémentaire sur celui-ci."));
                }
                if(u.containsTerr(TerritoireRepository.findTerrById(CarteTerrRepository.findCardTerrById(idArme2).getIdTerri()).getName())){
                    u.findTerr(TerritoireRepository.findTerrById(CarteTerrRepository.findCardTerrById(idArme2).getIdTerri()).getId()).changeArmy(2);
                    simpMessagingTemplate.convertAndSend(
                            "/queue/reply-user" + u.getSessionId(),
                            new HelloMessage("vous possédez le territoire " + TerritoireRepository.findTerrById(CarteTerrRepository.findCardTerrById(idArme2).getIdTerri()).getName() + " vous obtenez donc 2 armées supplémentaire sur celui-ci."));
                }
                if(u.containsTerr(TerritoireRepository.findTerrById(CarteTerrRepository.findCardTerrById(idArme2).getIdTerri()).getName())){
                    u.findTerr(TerritoireRepository.findTerrById(CarteTerrRepository.findCardTerrById(idArme3).getIdTerri()).getId()).changeArmy(2);
                    simpMessagingTemplate.convertAndSend(
                            "/queue/reply-user" + u.getSessionId(),
                            new HelloMessage("vous possédez le territoire " + TerritoireRepository.findTerrById(CarteTerrRepository.findCardTerrById(idArme3).getIdTerri()).getName() + " vous obtenez donc 2 armées supplémentaire sur celui-ci."));
                }
                List<List<Territoire>> temp = new ArrayList<>();
                for(int l=0;l<getPlayerById(sessionId).getTabTerri().size();l++){
                    temp.add(LinkRepository.findAllLinkTerritoire_name(getPlayerById(sessionId).getTabTerri().get(l).getName()));
                }
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply_myterritories-user" + sessionId,
                        new PlayerMessage(getPlayerById(sessionId).getTabTerri(),temp,getPlayerById(sessionId).getTabCont(),getPlayerById(sessionId).getMyCardsTerr(),getPlayerById(sessionId).getColor(),getPlayerById(sessionId).getMyCardObj().getDescription(),getPlayerById(sessionId).getNbArmeeTotal(),getPlayerById(sessionId).getArmeeAplacer()));
            }
        }else{
            simpMessagingTemplate.convertAndSend(
                    "/queue/reply-user" + u.getSessionId(),
                    new HelloMessage("vous ne pouvez pas échanger ces cartes."));
        }

    }

    /**
     * using to indicate at the player that is his turn and the time remaining for him to play
     *
     */

    public void playTurn(User u){
        this.rightToPlay=1;
        boolean oneMinuteWarning = false;
        boolean threeMinutesWarning = false;
        boolean twoMinutesWarning = false;
        simpMessagingTemplate.convertAndSend(
                "/queue/reply-user" + u.getSessionId(),
                new HelloMessage("vous avez 5 minutes pour effectuer vos actions de jeu"));
        this.rightToPlay=1;
        StopWatch watch = new StopWatch();
        watch.start();
        while(!u.getPassTurn()){
            if (watch.getTime(TimeUnit.MINUTES) == 2 && !threeMinutesWarning) {
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + u.getSessionId(),
                        new HelloMessage("il vous reste 3 minutes"));
                threeMinutesWarning=true;
            }
            if (watch.getTime(TimeUnit.MINUTES) == 3 && !twoMinutesWarning) {
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + u.getSessionId(),
                        new HelloMessage("il vous reste 2 minutes"));
                twoMinutesWarning=true;
            }
            if (watch.getTime(TimeUnit.MINUTES) == 4 && !oneMinuteWarning) {
                simpMessagingTemplate.convertAndSend(
                        "/queue/reply-user" + u.getSessionId(),
                        new HelloMessage("il vous reste 1 minutes"));
                oneMinuteWarning=true;
            }
            if (watch.getTime(TimeUnit.MINUTES) == 5) {
                u.changePassTurn();
            }
            try {
                Thread.sleep(10); // for 100 FPS
            } catch (InterruptedException ignore) {}
        }
        watch.stop();
        this.rightToPlay=0;
    }

    /**
     * using to check if the player win
     *
     */

    private boolean playerWin(User u,String color) {
        int compt = 0;
        if(u.getMyCardObj().getType().equals("conquerir")) {
            if(u.getMyCardObj().getNbTerri() != null) {
                if(u.getTabTerri().size() >= (Integer.parseInt(u.getMyCardObj().getNbTerri()))) {
                    if (u.getMyCardObj().getCond() == null) {
                        return true;
                    } else {
                        for (int i = 0; i < u.getTabTerri().size(); i++) {
                            if (u.getTabTerri().get(i).getNbArmee() >= 2) {
                                compt++;
                            }
                        }
                        if (compt == Integer.parseInt(u.getMyCardObj().getNbTerri())) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }else {
                if(u.containsCont(Integer.parseInt(u.getMyCardObj().getContinent1())) && u.containsCont(Integer.parseInt(u.getMyCardObj().getContinent2()))){
                    if(u.getMyCardObj().getCond() == null) {
                        return true;
                    }else {
                        if(u.getTabCont().size() >= 3) {
                            return true;
                        }else {
                            return false;
                        }
                    }
                }
            }
            return false;
        }else {
            if(u.getMyCardObj().getCouleur().equals(u.getColor())) {
                if(u.getTabTerri().size() >= Integer.parseInt(u.getMyCardObj().getNbTerri())) {
                    return true;
                }else {
                    return false;
                }
            }else {
                if(color.equals(u.getMyCardObj().getCouleur())) {
                    return true;
                }else {
                    if(checkIfColorExist(color)) {
                        return false;
                    }else {
                        if(u.getTabTerri().size() >= Integer.parseInt(u.getMyCardObj().getNbTerri())) {
                            return true;
                        }else {
                            return false;
                        }
                    }
                }
            }
        }
    }

    /**
     * using to check if a color of an army is still in game
     *
     */

    private boolean checkIfColorExist(String color) {
        for(int i=0;i<this.players.size();i++) {
            if(players.get(i).getColor().equals(color) && players.get(i).getIsAlive()) {
                return true;
            }
        }
        return false;
    }

    public void passTurn(String sessionId){
        getPlayerById(sessionId).changePassTurn();
    }
}
