package fr.alma.risk.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Representation of a player
 *
 * @author Léveillé Bastien & Hawa Nicolas & Cosson Alissa & Perez Damien
 * @version 0.1
 */

public class User {

	/**
	 * the color of player's armies
	 */

	private String color;

	/**
	 * the sessionId of the player
	 */

	private String sessionId;

	/**
	 * All the continents of the player
	 */

    private List<Continent> tabCont;

	/**
	 * All the territories of the player
	 */

    private List<Territoire> tabTerri;

	/**
	 * All the objective cards of the player
	 */

    private CarteObj myCardObj;

	/**
	 * All the territories cards of the player
	 */

	private List<CarteTerr> myCardsTerr;

	/**
	 * total number of armies owned by the player
	 */

    private int nbArmeeTotal;

	/**
	 * number of armies that the player need to place on his territory
	 */

    private int armeeAplacer;

	/**
	 * is the player still in the game ?
	 */

    private boolean isAlive;

	/**
	 * has the player passed his turn ?
	 */

    private boolean passTurn;

	/**
	 * the game where the player is
	 */

    private Game game;

	/**
	 * Constructor of a Player. Initializes most of the properties of this class.
	 *
	 * @param color        all players in the game.
	 * @param sessionId       all objective cards in the game.
	 */

	public User(String color, String sessionId) {
		this.color = color;
		this.sessionId = sessionId;
		this.nbArmeeTotal = 0;
		this.armeeAplacer = 0;
		this.isAlive = true;
		this.passTurn = false;
		this.tabCont = new ArrayList<>();
		this.tabTerri = new ArrayList<>();
		this.myCardsTerr = new ArrayList<>();
	}

	public String getColor() {
		return this.color;
	}

	public void setGame(Game g){
		this.game=g;
	}

	public Game getGame(){
		return this.game;
	}

	public String getSessionId(){
		return this.sessionId;
	}

	public List<Continent> getTabCont() {
		return this.tabCont;
	}

	public void addCont(Continent cont){
		this.tabCont.add(cont);
	}

	/**
	 * remove the continent from the tab of players's continents and return it according to the name of the continent
	 *
	 * @param nameCont the name of the continent
	 *
	 * @reutrn a Continent
	 */

	public Continent removeContByName(String nameCont){
		Continent res;
		for(int i=0;i<tabCont.size();i++){
			if(tabCont.get(i).getName().equals(nameCont)){
				res = tabCont.get(i);
				tabCont.remove(tabCont.get(i));
				return res;
			}
		}
		return null;
	}

	/**
	 * does the tab of player's continents contain this continent ? according to the continent ID
	 *
	 * @param idCont the id of the continent
	 *
	 * @return a boolean
	 */

	public boolean containsCont(int idCont){
		for(int i=0;i<tabCont.size();i++){
			if(tabCont.get(i).getId().equals(idCont)){
				return true;
			}
		}
		return false;
	}

	/**
	 * does the tab of player's continents contain this continent ? according to the continent name
	 *
	 * @param nameCont the name of the continent
	 *
	 * @return a boolean
	 */

	public boolean containsContByContName(String nameCont){
		for(int i=0;i<tabCont.size();i++){
			if(tabCont.get(i).getName().equals(nameCont)){
				return true;
			}
		}
		return false;
	}

	public List<Territoire> getTabTerri() {
		return this.tabTerri;
	}

	public void addTerr(Territoire t){
		this.tabTerri.add(t);
	}

	/**
	 * return the territory if he is in the player's territories tab, according to the territory name
	 * return null if he isn't
	 *
	 * @param terName the name of the territory
	 *
	 * @return a Territory
	 */

	public Territoire findTerrByName(String terName){
		for(int i=0;i<tabTerri.size();i++){
			if(tabTerri.get(i).getName().equals(terName)){
				return tabTerri.get(i);
			}
		}
		return null;
	}

	/**
	 * return the territory if he is in the player's territories tab, according to the territory ID
	 * return null if he isn't
	 *
	 * @param idTerr the id of the territory
	 *
	 * @return a Territory
	 */

	public Territoire findTerr(int idTerr){
		for(int i=0;i<tabTerri.size();i++){
			if(tabTerri.get(i).getId().equals(idTerr)){
				return tabTerri.get(i);
			}
		}
		return null;
	}

	/**
	 * does the tab of player's territories contain this territory ? according to the territory name
	 *
	 * @param terName the name of the territory
	 *
	 * @return a boolean
	 */

	public boolean containsTerr(String terName){
		for(int i=0;i<tabTerri.size();i++){
			if(tabTerri.get(i).getName().equals(terName)){
				return true;
			}
		}
		return false;
	}

	/**
	 * does the tab of player's territories contain this territory ? according to the territory ID
	 *
	 * @param terID the ID of the territory
	 *
	 * @return a boolean
	 */

	public boolean containsTerrById(int terID){
		for(int i=0;i<tabTerri.size();i++){
			if(tabTerri.get(i).getId().equals(terID)){
				return true;
			}
		}
		return false;
	}

	public void removeTerr(Territoire ter){
		this.tabTerri.remove(ter);
	}

	/**
	 * remove the territory from the tab of player's territories and return it, according to the territory id
	 *
	 * @param id the id of the territory
	 *
	 * @return a Territory
	 */

	public Territoire removeTerrByTerrId(int id){
		Territoire res;
		for(int i=0;i<tabTerri.size();i++){
			if(tabTerri.get(i).getId().equals(id)){
				res = tabTerri.get(i);
				tabTerri.remove(tabTerri.get(i));
				return res;
			}
		}
		return null;
	}

	public CarteObj getMyCardObj() {
		return this.myCardObj;
	}

	public void setCardObj(CarteObj card){
		this.myCardObj=card;
	}

	public List<CarteTerr> getMyCardsTerr(){
		return this.myCardsTerr;
	}

	public void addCardTerr(CarteTerr card){
		this.myCardsTerr.add(card);
	}

	/**
	 * does the tab of player's territory cards contain this card ? according to the card id
	 *
	 * @param id the id of the card
	 *
	 * @return a boolean
	 */

	public boolean searchCardTerr(int id){
		for(int i=0;i<this.myCardsTerr.size();i++){
			if(this.myCardsTerr.get(i).getIdTerri().equals(id)){
				return true;
			}
		}
		return false;
	}

	/**
	 * remove the card from the Player's territories cards tab and return it, according to the card weapon
	 *
	 * @param arme the weapon of the card
	 *
	 * @return a territory card
	 */

	public CarteTerr removeCardTerr(String arme){
		CarteTerr card;
		for(int i=0;i<this.myCardsTerr.size();i++){
			if(this.myCardsTerr.get(i).getArme().equals(arme)){
				card = this.myCardsTerr.get(i);
				this.myCardsTerr.remove(this.myCardsTerr.get(i));
				return card;
			}
		}
		return null;
	}

	/**
	 * how many times the tab of player's territories cards tab contain this weapon ? according to the weapon
	 *
	 * @param arme the weapon name
	 *
	 * @return an integer
	 */

	public int occurencyCard(String arme){
		int compt = 0;
		for(int i=0;i<this.myCardsTerr.size();i++){
			if(this.myCardsTerr.get(i).getArme().equals(arme)){
				compt++;
			}
		}
		return compt;
	}

	public int getNbArmeeTotal(){
		return this.nbArmeeTotal;
	}

	public int changeArmy(int nb){
		if(this.nbArmeeTotal+nb <= 0){
			return 0;
		}else {
			this.nbArmeeTotal+=nb;
			return this.nbArmeeTotal;
		}
	}

	public int getArmeeAplacer(){
		return this.armeeAplacer;
	}

	public void setArmeeAplacer(int i){
		this.armeeAplacer = i;
	}

	public void incrementArmeeAplacer(int i){
		this.armeeAplacer += i;
	}

	public boolean getIsAlive(){
		return this.isAlive;
	}

	public void elimine(){
		this.isAlive = false;
	}

	public boolean getPassTurn(){
		return this.passTurn;
	}

	public void changePassTurn(){
		if(this.passTurn == true){
			this.passTurn = false;
		}else{
			this.passTurn = true;
		}
	}
}
