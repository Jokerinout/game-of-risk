package fr.alma.risk.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Representation of objectif card.
 *
 * this class will be instance by spring according to the database
 *
 * @author Léveillé Bastien & Hawa Nicolas & Cosson Alissa & Perez Damien
 * @version 0.1
 */

@Entity
@Table(name="Carteobj")
public class CarteObj {

    /**
     * The id of the card
     */

    @Id
    @Column(name="idco")
    private int idCarteObj;

    /**
     * The description of the card, what you must do
     */

    @Column
    private String description;

    /**
     * The type of the card, 'conquérir' or 'détruire"
     */

    @Column
    private String type;

    /**
     * The number of territories that you need to have, can be null
     */

    @Column(name="nbterritoire")
    private String nbTerritoire;

    /**
     * The first Continent that you need to conquer, can be null
     */

    @Column(name="continent1")
    private String idContinent1;

    /**
     * The second Continent that you need to conquer, can be null
     */

    @Column(name="continent2")
    private String idContinent2;

    /**
     * The condition of the card, can be null
     */

    @Column
    private String cond;

    /**
     * The color that you need to destroy, can be null
     */

    @Column
    private String couleur;

    public int getIdCarteObj(){
        return this.idCarteObj;
    }

    public String getDescription(){
        return this.description;
    }

    public String getType(){
        return this.type;
    }

    public String getNbTerri() {
        return nbTerritoire;
    }

    public String getContinent1() {
        return idContinent1;
    }

    public String getContinent2() {
        return idContinent2;
    }

    public String getCond() {
        return cond;
    }

    public String getCouleur() {
        return couleur;
    }
}
