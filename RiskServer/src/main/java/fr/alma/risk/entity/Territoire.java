package fr.alma.risk.entity;

import javax.persistence.*;

/**
 * Representation of a Territory.
 *
 * this class will be instance by spring according to the database
 *
 * @author Léveillé Bastien & Hawa Nicolas & Cosson Alissa & Perez Damien
 * @version 0.1
 */

@Entity
@Table(name="Territoire")
public class Territoire {

    /**
     * The id of the territory
     */

    @Id
    private Integer idT;

    /**
     * The name of the territory
     */

    @Column(name="territoire_name")
    private String territoire_name;

    /**
     * The id of the continent which contains the territory
     *
     * one to one relation with Continent
     */

    @OneToOne
    @JoinColumn(name = "idcontinent")
    private Continent continent;

    /**
     * The number of armies on the territory
     *
     * this field isn't on the database
     */

    @Transient
    private int nbArmee;

    /**
     * The user on the territory
     *
     * this field isn't on the database
     */

    @Transient
    private User user;

    public Integer getId() {
        return this.idT;
    }

    public String getName() {
        return this.territoire_name;
    }

    public String getContinent(){
        return this.continent.getName();
    }

    public int getNbArmee(){
        return this.nbArmee;
    }

    public void setNbArmee(int nbArmee) {
        this.nbArmee = nbArmee;
    }

    public void changeArmy(int nbArmee){
        this.nbArmee += nbArmee;
    }

    public User getUser(){
        return this.user;
    }

    public void setUser(User u){
        this.user = u;
    }
}
