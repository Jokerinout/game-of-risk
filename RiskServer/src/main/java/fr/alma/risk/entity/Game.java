package fr.alma.risk.entity;

import java.util.Collections;
import java.util.List;

/**
 * Representation of a game
 *
 * @author Léveillé Bastien & Hawa Nicolas & Cosson Alissa & Perez Damien
 * @version 0.1
 */

public class Game {

    /**
     * All the players in the game
     */

    private List<User> players;

    /**
     * All the territories in the game
     */

    private List<CarteTerr> terrCards;

    /**
     * All the objective cards in the game
     */

    private List<CarteObj> objCards;

    /**
     * is the game finish ?
     */

    private boolean gameOver;

    /**
     * the turn
     */

    private int turn;

    /**
     * the player who has won
     */

    private String winner;

    /**
     * Constructor of Game. Initializes most of the properties of this class.
     *
     * @param players        all players in the game.
     * @param cardsObj       all objective cards in the game.
     * @param cardsTerr      all the territory cards in the game.
     */

    public Game(List<User> players,List<CarteObj> cardsObj, List<CarteTerr> cardsTerr){
        this.players = players;
        this.gameOver = false;
        this.turn = 0;
        this.terrCards = cardsTerr;
        this.objCards = cardsObj;
        this.winner = "";
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    /**
     * give the current player according to the turn
     *
     * @return a User
     */

    public User getCurrentPlayer(){
        if((((turn-1)%players.size()) == 0) || (((turn-1)%players.size()) == players.size())){
            return players.get(0);
        }else{
            return players.get(((turn-1) % this.getPlayers().size()));
        }
    }

    public void incrementTurn(){
        this.turn++;
    }

    public int getTurn() { return this.turn; }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(){
        this.gameOver = true;
    }

    public List<CarteObj> getObjCards() {
        return objCards;
    }

    public List<User> getPlayers() {
        return players;
    }

    /**
     * remove the player according to his sessionId
     *
     * @param sessionId the session id of the player
     */

    public void removePlayer(String sessionId){
        for(int i=0; i<this.getPlayers().size();i++){

            if(this.getPlayers().get(i).getSessionId() == sessionId){
                System.out.println("remove");
                this.getPlayers().remove(i);
            }
        }
    }

    /**
     * give the player according to his sessionId
     *
     * @param sessionId the session id of the player
     *
     * @return a User
     */

    public User getPlayerById(String sessionId){
        for(int i=0; i<this.getPlayers().size();i++){
            if(this.getPlayers().get(i).getSessionId() == sessionId){
                return this.getPlayers().get(i);
            }
        }
        return null;
    }

    public List<CarteTerr> getTerrCards() {
        return terrCards;
    }

    public void addTerrCard(CarteTerr t){
        this.terrCards.add(t);
    }

    public void shuffleTerritoire(){
        Collections.shuffle(this.terrCards);
    }

    public void shuffleObjectif(){
        Collections.shuffle(this.objCards);
    }

    /**
     * return the first objective card and remove it from the package
     *
     * @return a Card objective
     */

    public CarteObj pullaObjCard(){
        CarteObj card = this.objCards.get(0);
        this.objCards.remove(0);
        return card;
    }

    /**
     * return the first territory card and remove it from the package
     *
     * @return a territory card
     */

    public CarteTerr pullaTerrCard(){
        CarteTerr card = this.terrCards.get(0);
        this.terrCards.remove(0);
        return card;
    }
}
