package fr.alma.risk.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Representation of Territory card.
 *
 * this class will be instance by spring according to the database
 *
 * @author Léveillé Bastien & Hawa Nicolas & Cosson Alissa & Perez Damien
 * @version 0.1
 */

@Entity
@Table(name="Carteter")
public class CarteTerr {

    /**
     * The id of the card
     */

    @Id
    @Column(name="idcarteter")
    private int idCarteTer;

    /**
     * The id of the territory mention on the card
     */

    @Column(name="idterri")
    private Integer idTerri;

    /**
     * The weapon of the card
     */

    @Column
    private String arme;

    public int getIdCarteTer() {
        return idCarteTer;
    }

    public Integer getIdTerri() {
        return idTerri;
    }

    public String getArme() {
        return arme;
    }
}
