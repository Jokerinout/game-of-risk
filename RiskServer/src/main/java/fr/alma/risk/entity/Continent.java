package fr.alma.risk.entity;

import javax.persistence.*;

/**
 * Representation of Continent.
 *
 * this class will be instance by spring according to the database
 *
 * @author Léveillé Bastien & Hawa Nicolas & Cosson Alissa & Perez Damien
 * @version 0.1
 */

@Entity
@Table(name="Continent")
public class Continent {

    /**
     * The id of the continent
     */

    @Id
    private Integer idC;

    /**
     * The name of the continent
     */

    @Column(name="continent_name")
    private String continent_name;

    public Integer getId() {
        return this.idC;
    }

    public String getName() {
        return this.continent_name;
    }
}
