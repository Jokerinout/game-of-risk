package fr.alma.risk.entity;

import javax.persistence.*;

/**
 * Representation of the link between two territories.
 *
 * this class will be instance by spring according to the database
 *
 * @author Léveillé Bastien & Hawa Nicolas & Cosson Alissa & Perez Damien
 * @version 0.1
 */

@Entity
@Table(name="Lien")
public class Link {

    /**
     * The id of a particular link
     */

    @Id
    @Column(name = "idlien")
    private Integer idLien;

    /**
     * The id of the first territory
     *
     * one to one relation with Territoire
     */

    @OneToOne
    @JoinColumn(name = "idterritoire")
    private Territoire territoire_deb;

    /**
     * The id of the second territory
     *
     * one to one relation with Territoire
     */

    @OneToOne
    @JoinColumn(name = "idterritoire_lien")
    private Territoire territoire_fin;

    public int getIdLien(){
        return this.idLien;
    }

    public Territoire getTerritoire_deb() {
        return this.territoire_deb;
    }

    public Territoire getTerritoire_fin() {
        return this.territoire_fin;
    }
}
