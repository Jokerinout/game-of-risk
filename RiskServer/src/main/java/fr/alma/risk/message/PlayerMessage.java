package fr.alma.risk.message;

import fr.alma.risk.entity.CarteTerr;
import fr.alma.risk.entity.Continent;
import fr.alma.risk.entity.Territoire;

import java.util.ArrayList;
import java.util.List;

public class PlayerMessage {

    private List<TerritoryMessage> myTerr = new ArrayList<>();

    private String color;

    private String obj;

    private List<ContinentMessage> myCont = new ArrayList<>();

    private List<CarteTerrMessage> myCards = new ArrayList<>();

    private int nbArmee;

    private int armeeAplacer;

    public PlayerMessage(List<Territoire> terr, List<List <Territoire>> neighbors, List<Continent> cont, List<CarteTerr> cards, String col, String obj, int armies, int armiesToPlace){
        for(int i=0;i<terr.size();i++){
            TerritoryMessage TerrMessage = new TerritoryMessage(terr.get(i).getId(),terr.get(i).getName(),terr.get(i).getNbArmee(),terr.get(i).getUser().getColor(),neighbors.get(i));
            myTerr.add(TerrMessage);
        }
        for(int j=0;j<cont.size();j++){
            ContinentMessage contMessage = new ContinentMessage(cont.get(j).getId(),cont.get(j).getName());
            myCont.add(contMessage);
        }
        for(int z=0;z<cards.size();z++){
            CarteTerrMessage cardTerr = new CarteTerrMessage(cards.get(z).getIdCarteTer(), cards.get(z).getIdTerri(), cards.get(z).getArme());
            myCards.add(cardTerr);
        }
        this.color = col;
        this.obj = obj;
        this.nbArmee = armies;
        this.armeeAplacer = armiesToPlace;
    }

    public List<TerritoryMessage> getMyTerr() {
        return myTerr;
    }

    public String getColor() {
        return color;
    }

    public String getObj() {
        return obj;
    }

    public List<ContinentMessage> getMyCont() {
        return myCont;
    }

    public List<CarteTerrMessage> getMyCards() {
        return myCards;
    }

    public int getNbArmee() {
        return nbArmee;
    }

    public int getArmeeAplacer() {
        return armeeAplacer;
    }
}
