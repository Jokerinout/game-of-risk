package fr.alma.risk.message;

public class ContinentMessage {

    private int id;

    private String name;

    public ContinentMessage(int id, String name){
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
