package fr.alma.risk.message;

public class DefenseMessage {

    private String nameCountry;

    public DefenseMessage(String message){
        this.nameCountry = message;
    }

    public String getNameCountry() {
        return nameCountry;
    }
}
