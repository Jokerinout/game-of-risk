package fr.alma.risk.message;

public class CarteTerrMessage {

    private int id;

    private int idNameTerr;

    private String arme;

    public CarteTerrMessage(int id, int idName, String arme){
        this.id = id;
        this.idNameTerr = idName;
        this.arme = arme;
    }

    public int getId() {
        return id;
    }

    public int getNameTerr() {
        return idNameTerr;
    }

    public String getArme() {
        return arme;
    }
}
