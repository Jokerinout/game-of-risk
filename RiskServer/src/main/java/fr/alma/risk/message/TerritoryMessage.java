package fr.alma.risk.message;

import fr.alma.risk.entity.Territoire;

import java.util.ArrayList;
import java.util.List;

public class TerritoryMessage {

    private int id;

    private String name;

    private int nbArmee;

    private String color;

    private List<TerritoryMessage> neighbors = new ArrayList<>();

    public TerritoryMessage(int identifier, String nameTerr, int nb, String col, List<Territoire> terrs){
        this.id = identifier;
        this.name=nameTerr;
        this.nbArmee=nb;
        this.color=col;
        for(int i=0;i<terrs.size();i++){
            TerritoryMessage terrMessage = new TerritoryMessage(terrs.get(i).getId(),terrs.get(i).getName());
            neighbors.add(terrMessage);
        }
    }

    public TerritoryMessage(int identifier, String nameTerr, int nbArmee, String color) {
        this.id = identifier;
        this.name = nameTerr;
        this.nbArmee = nbArmee;
        this.color = color;
    }

    public TerritoryMessage(int identifier, String nameTerr) {
        this.id = identifier;
        this.name = nameTerr;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getNbArmee() {
        return nbArmee;
    }

    public String getColor() {
        return color;
    }

    public List<TerritoryMessage> getNeighbors() {
        return neighbors;
    }
}
