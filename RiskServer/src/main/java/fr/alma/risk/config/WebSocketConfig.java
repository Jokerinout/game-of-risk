package fr.alma.risk.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic/", "/queue/", "/user");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        // pour tester en local avec d'autres machines remplacez http://localhost:4200 par *
        registry.addEndpoint("/endpoint").setAllowedOrigins("http://localhost:4200").withSockJS();
    }

}
