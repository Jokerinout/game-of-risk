= Conception détaillée

Dans cette dernière partie nous allons décrire l'ensemble de la réalisation des composants du projet.

Nous commencerons par détailler le composant GameServer, puis WebClient.

== Le composant WebServer

image:DetailledDesignGameerver.png[]

Le composant se compose comme suit

* composants permettant de sauvegarder l'état du jeu :

** classe User : définit ce qu'est un utilisateur
** classe Territoire : définit ce qu'est un Territoire, est implémenté par la BDD
** classe CarteObj : définit ce qu'est une CarteObj, est implémenté par la BDD
** classe CarteTerritoire : définit ce qu'est une CarteTerritoire, est implémenté par la BDD
** classe link : définit ce qu'est un lien entre deux territoires, est implémenté par la BDD
** classe Continent : définit ce qu'est un continent, est implémenté par la BDD
** classe Game : définit ce qu'est un jeu.

* composants permettant l'interaction entre les clients et le serveur :

** interface NetworkExchanger : permet de recevoir les appels de méthodes de la part des clients et également d'en envoyer. Pour répondre aux exigences de ce projet, cette interface est implémentée par une classe gérant les échanges via WebSocket, mais il sera là encore facile d'ajouter d'autres formes de communication.
Relai toutes les classes MessageModels.

== Le composant WebClient

** classe Player : définit les informations d'un joueur (ses territoires, sa couleur etc)
** classe CarteObj : reprends les informations essentielles d'une carteObj
** classe CarteTerr : reprends les informations essentielles d'une carteTerr
** classe Territoire : reprends les informations essentielles d'un Territoire
** classe Continent : reprends les informations essentielles d'un Continent
