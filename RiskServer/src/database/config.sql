GRANT ALL ON risk.* TO 'Root'@'%';

CREATE TABLE continent (
	idC INTEGER NOT NULL AUTO_INCREMENT,
	continent_name VARCHAR(30) NOT NULL,
	PRIMARY KEY (idC)
);

CREATE TABLE territoire (
	idT INTEGER NOT NULL AUTO_INCREMENT,
	territoire_name VARCHAR(30) NOT NULL,
	idContinent INTEGER NOT NULL,
	PRIMARY KEY (idT),
	FOREIGN KEY (idContinent) REFERENCES continent(idC)
);

CREATE TABLE carteobj (
	idCo INTEGER NOT NULL AUTO_INCREMENT,
	description VARCHAR(500) NOT NULL,
	type VARCHAR(10) NOT NULL,
	nbTerritoire INTEGER,
	continent1 INTEGER,
	continent2 INTEGER,
	cond VARCHAR(20),
	couleur VARCHAR(10),
	PRIMARY KEY (idCo),
	FOREIGN KEY (continent1) REFERENCES continent(idC),
	FOREIGN KEY (continent2) REFERENCES continent(idC)
);

CREATE TABLE carteter (
	idCarteTer INTEGER NOT NULL AUTO_INCREMENT,
	idTerri INTEGER NOT NULL,
	arme VARCHAR(20) NOT NULL,
	PRIMARY KEY (idCarteTer),
	FOREIGN KEY (idTerri) REFERENCES territoire(idT)
);

CREATE TABLE lien (
	idLien INTEGER NOT NULL AUTO_INCREMENT,
	idTerritoire INTEGER NOT NULL,
	idTerritoire_lien INTEGER NOT NULL,
	PRIMARY KEY (idLien),
	FOREIGN KEY (idTerritoire) REFERENCES territoire(idT),
	FOREIGN KEY (idTerritoire_lien) REFERENCES territoire(idT)
);

INSERT INTO continent (continent_name) values ('Oceanie');
INSERT INTO continent (continent_name) values ('Amerique du sud');
INSERT INTO continent (continent_name) values ('Afrique');
INSERT INTO continent (continent_name) values ('Europe');
INSERT INTO continent (continent_name) values ('Amerique du nord');
INSERT INTO continent (continent_name) values ('Asie');

INSERT INTO territoire (territoire_name,idContinent) values ('Afghanistan',6);
INSERT INTO territoire (territoire_name,idContinent) values ('Afrique du Nord',3);
INSERT INTO territoire (territoire_name,idContinent) values ('Afrique du Sud',3);
INSERT INTO territoire (territoire_name,idContinent) values ('Afrique Orientale',3);
INSERT INTO territoire (territoire_name,idContinent) values ('Alaska',5);
INSERT INTO territoire (territoire_name,idContinent) values ('Alberta',5);
INSERT INTO territoire (territoire_name,idContinent) values ('Amerique Centrale',5);
INSERT INTO territoire (territoire_name,idContinent) values ('Argentine',2);
INSERT INTO territoire (territoire_name,idContinent) values ('Australie Occidentale',1);
INSERT INTO territoire (territoire_name,idContinent) values ('Australie Orientale',1);
INSERT INTO territoire (territoire_name,idContinent) values ('Bresil',2);
INSERT INTO territoire (territoire_name,idContinent) values ('Chine',6);
INSERT INTO territoire (territoire_name,idContinent) values ('Congo',3);
INSERT INTO territoire (territoire_name,idContinent) values ('Egypte',3);
INSERT INTO territoire (territoire_name,idContinent) values ('Etat de l Est',5);
INSERT INTO territoire (territoire_name,idContinent) values ('Etat de l Ouest',5);
INSERT INTO territoire (territoire_name,idContinent) values ('Europe du Nord',4);
INSERT INTO territoire (territoire_name,idContinent) values ('Europe du Sud',4);
INSERT INTO territoire (territoire_name,idContinent) values ('Europe Occidentale',4);
INSERT INTO territoire (territoire_name,idContinent) values ('Grande Bretagne',4);
INSERT INTO territoire (territoire_name,idContinent) values ('Groenland',5);
INSERT INTO territoire (territoire_name,idContinent) values ('Inde',6);
INSERT INTO territoire (territoire_name,idContinent) values ('Indonesie',1);
INSERT INTO territoire (territoire_name,idContinent) values ('Islande',4);
INSERT INTO territoire (territoire_name,idContinent) values ('Japon',6);
INSERT INTO territoire (territoire_name,idContinent) values ('Kamchatka',6);
INSERT INTO territoire (territoire_name,idContinent) values ('Madagascar',3);
INSERT INTO territoire (territoire_name,idContinent) values ('Mongolie',6);
INSERT INTO territoire (territoire_name,idContinent) values ('Moyen Orient',6);
INSERT INTO territoire (territoire_name,idContinent) values ('Nouvelle Guinee',1);
INSERT INTO territoire (territoire_name,idContinent) values ('Ontario',5);
INSERT INTO territoire (territoire_name,idContinent) values ('Oural',5);
INSERT INTO territoire (territoire_name,idContinent) values ('Perou',2);
INSERT INTO territoire (territoire_name,idContinent) values ('Quebec',5);
INSERT INTO territoire (territoire_name,idContinent) values ('Scandinavie',4);
INSERT INTO territoire (territoire_name,idContinent) values ('Siam',6);
INSERT INTO territoire (territoire_name,idContinent) values ('Siberie',6);
INSERT INTO territoire (territoire_name,idContinent) values ('Tchita',6);
INSERT INTO territoire (territoire_name,idContinent) values ('territoire du Nord Ouest',5);
INSERT INTO territoire (territoire_name,idContinent) values ('Ukraine',4);
INSERT INTO territoire (territoire_name,idContinent) values ('Venezuela',2);
INSERT INTO territoire (territoire_name,idContinent) values ('Yakoutie',6);

INSERT INTO lien (idTerritoire,idTerritoire_lien) values (5,39);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (5,6);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (5,26);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (39,5);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (39,6);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (39,31);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (39,21);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (21,39);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (21,34);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (21,31);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (21,24);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (6,5);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (6,39);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (6,31);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (6,16);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (31,6);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (31,39);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (31,21);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (31,34);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (31,15);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (31,16);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (34,31);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (34,21);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (34,15);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (16,6);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (16,31);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (16,15);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (16,7);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (15,16);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (15,31);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (15,34);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (15,7);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (7,16);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (7,15);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (7,41);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (41,7);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (41,11);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (41,33);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (33,41);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (33,11);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (33,8);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (11,41);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (11,33);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (11,8);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (11,2);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (8,33);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (8,11);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (24,35);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (24,20);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (24,21);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (35,24);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (35,20);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (35,17);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (35,40);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (40,35);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (40,17);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (40,18);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (40,29);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (40,1);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (40,32);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (20,24);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (20,35);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (20,17);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (20,19);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (17,20);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (17,40);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (17,35);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (17,18);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (17,19);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (19,20);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (19,17);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (19,18);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (19,2);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (18,19);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (18,17);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (18,40);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (18,29);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (18,14);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (18,2);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (2,11);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (2,19);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (2,18);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (2,14);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (2,4);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (2,13);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (14,2);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (14,18);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (14,29);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (14,4);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (4,2);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (4,14);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (4,13);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (4,3);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (4,29);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (4,27);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (13,2);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (13,4);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (13,3);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (3,4);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (3,13);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (3,27);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (27,4);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (27,3);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (32,40);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (32,37);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (32,1);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (37,32);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (37,42);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (37,38);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (37,28);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (37,12);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (42,37);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (42,26);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (42,38);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (26,5);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (26,42);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (26,38);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (26,28);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (26,25);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (38,37);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (38,42);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (38,26);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (38,28);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (1,40);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (1,32);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (1,12);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (1,22);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (1,29);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (28,37);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (28,38);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (28,26);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (28,25);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (28,12);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (25,26);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (25,28);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (12,1);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (12,32);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (12,37);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (12,28);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (12,36);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (12,22);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (29,40);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (29,18);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (29,14);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (29,4);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (29,22);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (29,1);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (22,29);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (22,1);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (22,12);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (22,36);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (36,23);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (36,22);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (36,12);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (23,36);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (23,30);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (23,9);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (30,23);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (30,9);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (30,10);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (9,10);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (9,23);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (9,30);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (10,9);
INSERT INTO lien (idTerritoire,idTerritoire_lien) values (10,30);

INSERT INTO carteter (idTerri,arme) values (1,'fantassin');
INSERT INTO carteter (idTerri,arme) values (2,'fantassin');
INSERT INTO carteter (idTerri,arme) values (3,'canon');
INSERT INTO carteter (idTerri,arme) values (4,'canon');
INSERT INTO carteter (idTerri,arme) values (5,'fantassin');
INSERT INTO carteter (idTerri,arme) values (6,'fantassin');
INSERT INTO carteter (idTerri,arme) values (7,'cavalier');
INSERT INTO carteter (idTerri,arme) values (8,'fantassin');
INSERT INTO carteter (idTerri,arme) values (9,'canon');
INSERT INTO carteter (idTerri,arme) values (10,'fantassin');
INSERT INTO carteter (idTerri,arme) values (11,'canon');
INSERT INTO carteter (idTerri,arme) values (12,'cavalier');
INSERT INTO carteter (idTerri,arme) values (13,'cavalier');
INSERT INTO carteter (idTerri,arme) values (14,'fantassin');
INSERT INTO carteter (idTerri,arme) values (15,'canon');
INSERT INTO carteter (idTerri,arme) values (16,'fantassin');
INSERT INTO carteter (idTerri,arme) values (17,'cavalier');
INSERT INTO carteter (idTerri,arme) values (18,'cavalier');
INSERT INTO carteter (idTerri,arme) values (19,'fantassin');
INSERT INTO carteter (idTerri,arme) values (20,'cavalier');
INSERT INTO carteter (idTerri,arme) values (21,'cavalier');
INSERT INTO carteter (idTerri,arme) values (22,'fantassin');
INSERT INTO carteter (idTerri,arme) values (23,'cavalier');
INSERT INTO carteter (idTerri,arme) values (24,'fantassin');
INSERT INTO carteter (idTerri,arme) values (25,'fantassin');
INSERT INTO carteter (idTerri,arme) values (26,'cavalier');
INSERT INTO carteter (idTerri,arme) values (27,'fantassin');
INSERT INTO carteter (idTerri,arme) values (28,'canon');
INSERT INTO carteter (idTerri,arme) values (29,'canon');
INSERT INTO carteter (idTerri,arme) values (30,'cavalier');
INSERT INTO carteter (idTerri,arme) values (31,'cavalier');
INSERT INTO carteter (idTerri,arme) values (32,'cavalier');
INSERT INTO carteter (idTerri,arme) values (33,'cavalier');
INSERT INTO carteter (idTerri,arme) values (34,'canon');
INSERT INTO carteter (idTerri,arme) values (35,'canon');
INSERT INTO carteter (idTerri,arme) values (36,'canon');
INSERT INTO carteter (idTerri,arme) values (37,'canon');
INSERT INTO carteter (idTerri,arme) values (38,'fantassin');
INSERT INTO carteter (idTerri,arme) values (39,'canon');
INSERT INTO carteter (idTerri,arme) values (40,'canon');
INSERT INTO carteter (idTerri,arme) values (41,'canon');
INSERT INTO carteter (idTerri,arme) values (42,'cavalier');

INSERT INTO carteobj (description,type,nbTerritoire,continent1,continent2,cond,couleur) values ('Vous devez conquerir 18 territoires et occuper chacun d eux avec deux armees au moins','conquerir',18,Null,Null,'pays min 2 armee',Null);
INSERT INTO carteobj (description,type,nbTerritoire,continent1,continent2,cond,couleur) values ('Vous devez conquerir en totalite l Amerique du nord et l Afrique','conquerir',Null,5,3,Null,Null);
INSERT INTO carteobj (description,type,nbTerritoire,continent1,continent2,cond,couleur) values ('Vous devez conquerir en totalite l europe et l Amerique du sud plus un troisieme continent au choix','conquerir',Null,4,2,'+1 continent',Null);
INSERT INTO carteobj (description,type,nbTerritoire,continent1,continent2,cond,couleur) values ('Vous devez conquerir en totalite l europe et l Oceanie plus un troisieme continent au choix','conquerir',Null,4,1,'+1 continent',Null);
INSERT INTO carteobj (description,type,nbTerritoire,continent1,continent2,cond,couleur) values ('Vous devez conquerir 24 territoires au choix','conquerir',24,Null,Null,Null,Null);
INSERT INTO carteobj (description,type,nbTerritoire,continent1,continent2,cond,couleur) values ('Vous devez conquerir en totalite l Amerique du nord et l Oceanie','conquerir',Null,5,1,Null,Null);
INSERT INTO carteobj (description,type,nbTerritoire,continent1,continent2,cond,couleur) values ('Vous devez conquerir en totalite l Asie et l Afrique','conquerir',Null,6,3,Null,Null);
INSERT INTO carteobj (description,type,nbTerritoire,continent1,continent2,cond,couleur) values ('Vous devez conquerir en totalite l Asie et l Amerique du sud','conquerir',Null,6,2,Null,Null);
INSERT INTO carteobj (description,type,nbTerritoire,continent1,continent2,cond,couleur) values ('Vous devez detruire les armees jaunes. Si vous etes vous meme le proprietaire des armees jaunes ou si le joueur qui en est proprietaire est elimine par un autre joueur, votre but devient automatiquement de conquerir 24 territoires','detruire',24,Null,Null,Null,'jaune');
INSERT INTO carteobj (description,type,nbTerritoire,continent1,continent2,cond,couleur) values ('Vous devez detruire les armees rouges. Si vous etes vous meme le proprietaire des armees rouges ou si le joueur qui en est proprietaire est elimine par un autre joueur, votre but devient automatiquement de conquerir 24 territoires','detruire',24,Null,Null,Null,'rouge');
INSERT INTO carteobj (description,type,nbTerritoire,continent1,continent2,cond,couleur) values ('Vous devez detruire les armees bleues. Si vous etes vous meme le proprietaire des armees bleues ou si le joueur qui en est proprietaire est elimine par un autre joueur, votre but devient automatiquement de conquerir 24 territoires','detruire',24,Null,Null,Null,'bleues');
INSERT INTO carteobj (description,type,nbTerritoire,continent1,continent2,cond,couleur) values ('Vous devez detruire les armees noires. Si vous etes vous meme le proprietaire des armees noires ou si le joueur qui en est proprietaire est elimine par un autre joueur, votre but devient automatiquement de conquerir 24 territoires','detruire',24,Null,Null,Null,'noires');
INSERT INTO carteobj (description,type,nbTerritoire,continent1,continent2,cond,couleur) values ('Vous devez detruire les armees violettes. Si vous etes vous meme le proprietaire des armees violettes ou si le joueur qui en est proprietaire est elimine par un autre joueur, votre but devient automatiquement de conquerir 24 territoires','detruire',24,Null,Null,Null,'violettes');
INSERT INTO carteobj (description,type,nbTerritoire,continent1,continent2,cond,couleur) values ('Vous devez detruire les armees vertes. Si vous etes vous meme le proprietaire des armees vertes ou si le joueur qui en est proprietaire est elimine par un autre joueur, votre but devient automatiquement de conquerir 24 territoires','detruire',24,Null,Null,Null,'vertes');
