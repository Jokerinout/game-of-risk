import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TerritoireComponent } from './territoire/territoire.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { PlayerComponent } from './player/player.component';
import { ContinentComponent } from './continent/continent.component';
import { CarteTerrComponent } from './carte-terr/carte-terr.component';

@NgModule({
  declarations: [
    AppComponent,
    TerritoireComponent,
    WelcomeComponent,
    PlayerComponent,
    ContinentComponent,
    CarteTerrComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
