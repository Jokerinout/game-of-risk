import { Component } from '@angular/core';
import {Stomp} from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import {Territoire} from './territoire/territoire.model';
import {Player} from './player/player.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Game Of Risk';
  text: string;
  greetings: string[] = [];
  territoiries: Territoire[] = [];
  myGame: Player;
  inputNbChangeArmy: string;
  inputNbAttack: string;
  inputNbDef: string;
  inputNbMove: string;
  inputArme1: string;
  inputArme2: string;
  inputArme3: string;
  inputCountryDef: string;
  inputCountryToAttack: string;
  inputCountryToMoove: string;
  disabled = true;
  ready = false;
  initDisable = false;
  selectedCountry: string;
  selectedArmy: number;
  neighborsToAttack = [];
  neighborsToMoove = [];

  private stompClient = null;

  constructor() {
      this.myGame = new Player();
  }

  setConnected(connected: boolean) {
    this.disabled = !connected;

    if (connected) {
      this.greetings = [];
    }
  }

  changeColor( color: string ) {
      switch (color) {
          case 'jaune':
            return 'rgba(255, 238, 82, 0.57)';
          case 'rouge':
            return 'rgba(191, 36, 36, 0.57)';
          case 'bleues':
            return 'rgba(79, 91, 255, 0.54)';
          case 'noires':
            return 'rgba(37, 35, 33, 0.7)';
          case 'violettes':
            return 'rgba(251, 55, 243, 0.45)';
          case 'vertes':
            return 'rgba(104, 238, 82, 0.68)';
        }
  }

  connect() {
    // remplacer le "http://localhost:8080/endpoint" par "http://ipdemamachinesurleréseaulocal:8080/endpoint"
    // pour pouvoir jouer sur plusieurs machines sur un même réseau local
    const socket = new SockJS('http://localhost:8080/endpoint');
    this.stompClient = Stomp.over(socket);
    this.stompClient.connect({}, (frame) => {
        this.setConnected(true);
        this.stompClient.subscribe('/user/queue/reply', (resp) => {
            this.showGreeting(JSON.parse(resp.body).name);
        });
        this.stompClient.subscribe('/user/queue/reply_sendterritory', (resp) => {
            this.pushTerritory(JSON.parse(resp.body));
            this.territoiries.sort((a, b) => {
                return a.name.localeCompare(b.name);
            });
        });
        this.stompClient.subscribe('/user/queue/reply_askingDefence', (resp) => {
            this.inputCountryDef = JSON.parse(resp.body).nameCountry;
            this.inputNbDef = '1';
        });
        this.stompClient.subscribe('/user/queue/reply_myterritories', (resp) => {
            this.myGame.myCards = [];
            this.myGame.myContinent = [];
            JSON.parse(resp.body).myCont.forEach((elmt) => {
                const tempo = { id: elmt.id, name: elmt.name };
                this.myGame.myContinent.push(tempo);
            });
            JSON.parse(resp.body).myCards.forEach((element) => {
                const temp = { id: element.id, idTerr: element.nameTerr, arme: element.arme };
                this.myGame.myCards.push(temp);
            });
            this.myGame.myTerritories = JSON.parse(resp.body).myTerr;
            let i = 0;
            for (let elt of JSON.parse(resp.body).myTerr) {
                this.myGame.myTerritories[i].neighbors.push(elt.neighbors);
                i++;
            }
            this.myGame.myTerritories.sort((a, b) => {
                return a.name.localeCompare(b.name);
            });
            this.myGame.color = JSON.parse(resp.body).color;
            this.myGame.carteObj = JSON.parse(resp.body).obj;
            this.myGame.nbArmee = JSON.parse(resp.body).nbArmee;
            this.myGame.armeeAplacer = JSON.parse(resp.body).armeeAplacer;
        });
    });
  }

  containsTerr(name: string) {
      let found = false;
      this.myGame.myTerritories.forEach((elt) => {
          if (elt.name === name) {
              found = true;
          }
      });
      return found;
  }

  showNeighbors() {
      this.neighborsToAttack = [];
      this.neighborsToMoove = [];
      if (this.selectedCountry !== undefined && this.containsTerr(this.selectedCountry) && this.myGame.myTerritories !== undefined) {
          this.myGame.myTerritories.forEach((elt) => {
              if (elt.name === this.selectedCountry) {
                  elt.neighbors.forEach((nei) => {
                      if (this.containsTerr(nei.name)) {
                          this.neighborsToMoove.push(nei);
                      } else {
                          this.neighborsToAttack.push(nei);
                      }
                  });
              }
          });
          if (this.neighborsToAttack.length > 0 && this.neighborsToMoove.length > 0) {
              this.inputCountryToAttack = this.neighborsToAttack[0].name;
              this.inputCountryToMoove = this.neighborsToMoove[0].name;
          }
      }
  }

  showCountry(id: string) {
      this.text = 'Pays: ' + id + ', Nb Armées: ' + this.findNbArmy(id);
  }

  highlightCountry(id: string) {
      const outline = document.getElementById(id).getAttribute( 'd' );
      const hilite = document.getElementById( 'hilite' );
      hilite.setAttribute( 'd', outline );
      this.selectedCountry = id;
      this.selectedArmy = this.findNbArmy(id);
      this.showNeighbors();
  }

  init() {
    this.stompClient.send('/connect', {});
    this.initDisable = true;
  }

  pushTerritory(terr: Territoire) {
      let compt = 0;
      if (this.territoiries.length === 0) {
          this.territoiries.push(terr);
      } else {
          this.territoiries.forEach((elt) => {
              if (elt.name === terr.name) {
                 elt.color = terr.color;
                 elt.nbArmee = terr.nbArmee;
                 compt++;
             }
          });
          if (compt === 0) {
              this.territoiries.push(terr);
          }
      }
      const country = document.getElementById(terr.name);
      country.setAttribute('fill', this.changeColor(terr.color));
  }

  findNbArmy(terName: string) {
      let res;
      this.territoiries.forEach((elt) => {
          if (elt.name === terName) {
              res = elt.nbArmee;
          }
      });
      return res;
  }

  findIdTerr(terName: string) {
      let res;
      this.territoiries.forEach((elt) => {
          if (elt.name === terName) {
              res = elt.id;
          }
      });
      return res;
  }

  findNameTer(terId: string) {
      let res;
      this.territoiries.forEach((elt) => {
          if (elt.id === terId) {
              res = elt.name;
          }
      });
      return res;
  }

  changeCountryToAttack(id) {
      this.inputCountryToAttack = (document.getElementById(id) as HTMLTextAreaElement).value;
  }

  changeCountryToMoove(id) {
      this.inputCountryToMoove = (document.getElementById(id) as HTMLTextAreaElement).value;
  }

  changeNbAttackSubmit(id) {
      this.inputNbAttack = (document.getElementById(id) as HTMLTextAreaElement).value;
  }

  changeNbDefSubmit(id) {
      this.inputNbDef = (document.getElementById(id) as HTMLTextAreaElement).value;
  }

  changeNbMooveSubmit(id) {
      this.inputNbMove = (document.getElementById(id) as HTMLTextAreaElement).value;
  }

  changeNbChangeArmySubmit(id) {
      this.inputNbChangeArmy = (document.getElementById(id) as HTMLTextAreaElement).value;
  }

  changeWeapon(id) {
      if (id === 'first') {
          this.inputArme1 = (document.getElementById(id) as HTMLTextAreaElement).value;
      }
      if (id === 'second') {
          this.inputArme2 = (document.getElementById(id) as HTMLTextAreaElement).value;
      }
      if (id === 'third') {
          this.inputArme3 = (document.getElementById(id) as HTMLTextAreaElement).value;
      }
  }

  findIdCard(cardName: string) {
      let res;
      const str = cardName.split(' ');
      this.myGame.myCards.forEach((elt) => {
          if (elt.idTerr === this.findIdTerr(str[0]) && elt.arme === str[1]) {
              res = elt.id;
          }
      });
      return res;
  }

  readyToPlay() {
    this.stompClient.send('/ready', {});
    this.ready = true;
  }

  showGreeting(message) {
    if (this.greetings.length === 10) {
        this.greetings.shift();
    }
    this.greetings.push(message);
  }

  disconnect() {
    if (this.stompClient !== null) {
        this.stompClient.disconnect();
    }
    console.log('Disconnected');
  }

  sendArmy() {
    event.preventDefault();
    if (this.selectedCountry !== undefined && this.inputNbChangeArmy !== undefined) {
        const message = { idTerr: this.findIdTerr(this.selectedCountry), nb: this.inputNbChangeArmy};
        this.stompClient.send('/setupArmy', {}, JSON.stringify(message));
    }
  }

  changeCard() {
    event.preventDefault();
    // tslint:disable-next-line:max-line-length
    if (this.findIdCard(this.inputArme1) !== this.findIdCard(this.inputArme2) && this.findIdCard(this.inputArme1) !== this.findIdCard(this.inputArme3) && this.findIdCard(this.inputArme2) !== this.findIdCard(this.inputArme3)) {
        // tslint:disable-next-line:max-line-length
        const message = {arme1: this.findIdCard(this.inputArme1), arme2: this.findIdCard(this.inputArme2), arme3: this.findIdCard(this.inputArme3)};
        this.stompClient.send('/changeTerrCard', {}, JSON.stringify(message));
    } else {
        this.greetings.push('choissisez 3 cartes différentes');
    }
  }

  passTurn() {
    event.preventDefault();
    this.stompClient.send('/passTurn', {});
  }

  attack() {
    event.preventDefault();
    if (this.selectedCountry !== undefined && this.inputCountryToAttack !== undefined && this.inputNbAttack !== undefined) {
        // tslint:disable-next-line:max-line-length
        const message = { idDep: this.findIdTerr(this.selectedCountry), idCible: this.findIdTerr(this.inputCountryToAttack), nb: this.inputNbAttack };
        this.stompClient.send('/attackTerritory', {}, JSON.stringify(message));
    }
  }

  move() {
    event.preventDefault();
    if (this.selectedCountry !== undefined && this.inputCountryToMoove !== undefined) {
        // tslint:disable-next-line:max-line-length
        const message = { idDep: this.findIdTerr(this.selectedCountry), idCible: this.findIdTerr(this.inputCountryToMoove), nb: this.inputNbMove};
        this.stompClient.send('/moveArmies', {}, JSON.stringify(message));
    }
  }

  def() {
   event.preventDefault();
   if (this.inputNbDef !== undefined) {
       const message = { terri: this.findIdTerr(this.inputCountryDef), nb: this.inputNbDef};
       this.stompClient.send('/getDef', {}, JSON.stringify(message));
   }
   this.inputNbDef = undefined;
  }

}
