export class Territoire {
    id: string;
    name: string;
    nbArmee: number;
    color: string;
    neighbors: Territoire[] = [];
}
