import {Territoire} from '../territoire/territoire.model';
import {Continent} from '../continent/continent.model';
import {CarteTerr} from '../carte-terr/carte-terr.model';

export class Player {
    myTerritories: Territoire[];
    color: string;
    carteObj: string;
    myContinent: Continent[] = [];
    myCards: CarteTerr[] = [];
    nbArmee: string;
    armeeAplacer: string;
}
