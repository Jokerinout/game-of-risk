import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarteTerrComponent } from './carte-terr.component';

describe('CarteTerrComponent', () => {
  let component: CarteTerrComponent;
  let fixture: ComponentFixture<CarteTerrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarteTerrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarteTerrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
